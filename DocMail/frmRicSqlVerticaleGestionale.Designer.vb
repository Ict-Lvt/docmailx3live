<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRicSqlVerticaleGestionale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GridMain = New System.Windows.Forms.DataGridView()
        Me.nome_chiave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descrizione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stringa_sql = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtRicerca = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.GridMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridMain
        '
        Me.GridMain.AllowUserToAddRows = False
        Me.GridMain.AllowUserToDeleteRows = False
        Me.GridMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridMain.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nome_chiave, Me.descrizione, Me.stringa_sql})
        Me.GridMain.Location = New System.Drawing.Point(13, 39)
        Me.GridMain.Name = "GridMain"
        Me.GridMain.ReadOnly = True
        Me.GridMain.Size = New System.Drawing.Size(745, 386)
        Me.GridMain.TabIndex = 18
        '
        'nome_chiave
        '
        Me.nome_chiave.DataPropertyName = "nome_chiave"
        Me.nome_chiave.HeaderText = "Nome Chiave"
        Me.nome_chiave.Name = "nome_chiave"
        Me.nome_chiave.ReadOnly = True
        Me.nome_chiave.Width = 150
        '
        'descrizione
        '
        Me.descrizione.DataPropertyName = "descrizione"
        Me.descrizione.HeaderText = "Descrizione"
        Me.descrizione.Name = "descrizione"
        Me.descrizione.ReadOnly = True
        Me.descrizione.Width = 150
        '
        'stringa_sql
        '
        Me.stringa_sql.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.stringa_sql.DataPropertyName = "stringa_sql"
        Me.stringa_sql.HeaderText = "Stringa Sql"
        Me.stringa_sql.Name = "stringa_sql"
        Me.stringa_sql.ReadOnly = True
        '
        'txtRicerca
        '
        Me.txtRicerca.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRicerca.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRicerca.Location = New System.Drawing.Point(92, 12)
        Me.txtRicerca.MaxLength = 200
        Me.txtRicerca.Name = "txtRicerca"
        Me.txtRicerca.Size = New System.Drawing.Size(666, 21)
        Me.txtRicerca.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 16)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Ricerca . . . . . ."
        '
        'frmRicSqlVerticaleGestionale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(771, 437)
        Me.Controls.Add(Me.GridMain)
        Me.Controls.Add(Me.txtRicerca)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmRicSqlVerticaleGestionale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmRicSqlVerticaleGestionale"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GridMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GridMain As System.Windows.Forms.DataGridView
    Friend WithEvents nome_chiave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents descrizione As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stringa_sql As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtRicerca As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
