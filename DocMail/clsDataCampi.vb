#Region "String"
Public Class clsFieldString

    Private msDataField As String = ""
    Private msValore As String = ""
    Private mlMaxLength As Long = 0
    Private mbChanged As Boolean = False

    Public Property DataField() As String
        Get
            Return msDataField
        End Get
        Set(ByVal value As String)
            msDataField = value
        End Set
    End Property

    Public Property Valore() As String
        Get
            Return msValore
        End Get
        Set(ByVal value As String)
            If mlMaxLength > 0 Then
                msValore = GetMaxDim(value, mlMaxLength)
            Else
                msValore = value
            End If
            mbChanged = True
        End Set
    End Property

    Public ReadOnly Property ValoreSQL() As String
        Get
            Return GetSQLStr(msValore)
        End Get
    End Property

    Public Property Changed() As Boolean
        Get
            Return mbChanged
        End Get
        Set(ByVal value As Boolean)
            mbChanged = value
        End Set
    End Property

    Public Property MaxLength() As Long
        Get
            Return mlMaxLength
        End Get
        Set(ByVal value As Long)
            mlMaxLength = value
        End Set
    End Property

    Public Sub Clear()
        msValore = ""
        mbChanged = False
    End Sub

End Class

#End Region

#Region "Long"
'----------------------------------------------------------------------------------------------------
' LONG
'----------------------------------------------------------------------------------------------------
Public Class clsFieldLong

    Private msDataField As String = ""
    Private mlValore As Long
    Private mbChanged As Boolean = False

    Public Property DataField() As String
        Get
            Return msDataField
        End Get
        Set(ByVal value As String)
            msDataField = value
        End Set
    End Property

    Public Property Valore() As Long
        Get
            Return mlValore
        End Get
        Set(ByVal value As Long)
            mlValore = value
            mbChanged = True
        End Set
    End Property

    Public ReadOnly Property ValoreSQL() As Long
        Get
            Return mlValore
        End Get
    End Property

    Public Property Changed() As Boolean
        Get
            Return mbChanged
        End Get
        Set(ByVal value As Boolean)
            mbChanged = value
        End Set
    End Property

    Public ReadOnly Property MaxLength() As Long
        Get
            Return 10
        End Get
    End Property

    Public Sub Clear()
        mlValore = 0
        mbChanged = False
    End Sub

End Class

#End Region

#Region "Double"
'----------------------------------------------------------------------------------------------------
' LONG
'----------------------------------------------------------------------------------------------------
Public Class clsFieldDouble

    Private msDataField As String = ""
    Private mdValore As Double
    Private mbChanged As Boolean = False

    Public Property DataField() As String
        Get
            Return msDataField
        End Get
        Set(ByVal value As String)
            msDataField = value
        End Set
    End Property

    Public Property Valore() As Double
        Get
            Return mdValore
        End Get
        Set(ByVal value As Double)
            mdValore = value
            mbChanged = True
        End Set
    End Property

    Public ReadOnly Property ValoreSQL() As String
        Get
            Return GetSQLFloat(mdValore)
        End Get
    End Property

    Public Property Changed() As Boolean
        Get
            Return mbChanged
        End Get
        Set(ByVal value As Boolean)
            mbChanged = value
        End Set
    End Property

    Public ReadOnly Property MaxLength() As Long
        Get
            Return 10
        End Get
    End Property

    Public Sub Clear()
        mdValore = 0
        mbChanged = False
    End Sub

End Class

#End Region

#Region "Date"
'----------------------------------------------------------------------------------------------------
' DATE
'----------------------------------------------------------------------------------------------------
Public Class clsFieldDate

    Private msDataField As String = ""
    Private mdtValore As Date
    Private mbChanged As Boolean = False

    Public Property DataField() As String
        Get
            Return msDataField
        End Get
        Set(ByVal value As String)
            msDataField = value
        End Set
    End Property

    Public Property Valore() As Date
        Get
            Return mdtValore
        End Get
        Set(ByVal value As Date)
            If Not IsNothing(value) Then
                If Not IsDate(value) Then
                    value = "01/01/1900"
                End If
                mdtValore = value
                mbChanged = True
            End If
        End Set
    End Property

    Public ReadOnly Property ValoreSQL() As String
        Get
            Return GetSQLDate(mdtValore)
        End Get
    End Property

    Public Property Changed() As Boolean
        Get
            Return mbChanged
        End Get
        Set(ByVal value As Boolean)
            mbChanged = value
        End Set
    End Property

    Public ReadOnly Property MaxLength() As Long
        Get
            Return 10
        End Get
    End Property

    Public Sub Clear()
        mdtValore = "01/01/1900"
        mbChanged = False
    End Sub

End Class

#End Region

#Region "Time"
'----------------------------------------------------------------------------------------------------
' TIME
'----------------------------------------------------------------------------------------------------
Public Class clsFieldTime

    Private msDataField As String = ""
    Private mdtValore As Date
    Private mbChanged As Boolean = False

    Public Property DataField() As String
        Get
            Return msDataField
        End Get
        Set(ByVal value As String)
            msDataField = value
        End Set
    End Property

    Public Property Valore() As Date
        Get
            Return mdtValore
        End Get
        Set(ByVal value As Date)
            If Not IsNothing(value) Then
                If Not IsDate(value) Then
                    value = Format("00:00:00", "dd/MM/yyyy HH:mm:ss")
                End If
                mdtValore = Format(value, "dd/MM/yyyy HH:mm:ss")
                mbChanged = True
            End If
        End Set
    End Property

    Public ReadOnly Property ValoreSQL() As String
        Get
            Return GetSQLTime(mdtValore)
        End Get
    End Property

    Public Property Changed() As Boolean
        Get
            Return mbChanged
        End Get
        Set(ByVal value As Boolean)
            mbChanged = value
        End Set
    End Property

    Public ReadOnly Property MaxLength() As Long
        Get
            Return 8
        End Get
    End Property

    Public Sub Clear()
        mdtValore = "01/01/1900 00:00:00"
        mbChanged = False
    End Sub

End Class

#End Region

#Region "Bit"
'----------------------------------------------------------------------------------------------------
' LONG
'----------------------------------------------------------------------------------------------------
Public Class clsFieldBit

    Private msDataField As String = ""
    Private mbValore As Boolean
    Private mbChanged As Boolean = False

    Public Property DataField() As String
        Get
            Return msDataField
        End Get
        Set(ByVal value As String)
            msDataField = value
        End Set
    End Property

    Public Property Valore() As Boolean
        Get
            Return mbValore
        End Get
        Set(ByVal value As Boolean)
            mbValore = value
            mbChanged = True
        End Set
    End Property

    Public ReadOnly Property ValoreSQL() As Long
        Get
            If mbValore = True Then
                Return 1
            Else
                Return 0
            End If
        End Get
    End Property

    Public Property Changed() As Boolean
        Get
            Return mbChanged
        End Get
        Set(ByVal value As Boolean)
            mbChanged = value
        End Set
    End Property

    Public ReadOnly Property MaxLength() As Long
        Get
            Return 1
        End Get
    End Property

    Public Sub Clear()
        mbValore = False
        mbChanged = False
    End Sub

End Class

#End Region