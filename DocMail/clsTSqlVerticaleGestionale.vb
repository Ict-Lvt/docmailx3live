Public Class clsTSqlVerticaleGestionale
    Private msTabella As String = "TSqlVerticaleGestionale"
    Private msNomeForm As String = "clsTSqlVerticaleGestionale"
    Public mDBConn As New SqlClient.SqlConnection(g_sConnStringTecno)
    Private mDataSet As New DataSet("DS" & msTabella)
    Private sSQL As String
    Private mlRows As Long

    ' per ogni campo della tabella dichiaro una variabile
    Private msNomeChiave As New clsFieldString '50
    Private msDescrizione As New clsFieldString '250
    Private msStringaSql As New clsFieldString 'text


    ' ... e scrivo le property get/set

    Public Property NomeChiave() As clsFieldString
        Get
            Return msNomeChiave
        End Get
        Set(ByVal value As clsFieldString)
            msNomeChiave = value
        End Set
    End Property

    Public Property Descrizione() As clsFieldString
        Get
            Return msDescrizione
        End Get
        Set(ByVal value As clsFieldString)
            msDescrizione = value
        End Set
    End Property

    Public Property StringaSql() As clsFieldString
        Get
            Return msStringaSql
        End Get
        Set(ByVal value As clsFieldString)
            msStringaSql = value
        End Set
    End Property

    'apro la connessione al db
    Public Sub OpenConnection()

        On Error GoTo gesterr

        If mDBConn.State = ConnectionState.Closed Then
            mDBConn.Open()
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'chiudo la connessione al db
    Public Sub CloseConnection()

        On Error GoTo gesterr

        mDBConn.Close()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Public Property Rows() As Long
        Get
            Return mlRows
        End Get
        Set(ByVal value As Long)
            If mlRows < 1 Then mlRows = 1
            mlRows = value
            ValorizzaCampi()
        End Set
    End Property

    ' mi restituisce quante righe ha trovato nel dataset
    Public ReadOnly Property RowsCount() As Long
        Get
            Return mDataSet.Tables(msTabella).Rows.Count
        End Get
    End Property

    'svuoto i campi
    Private Sub PulisciCampi()

        On Error GoTo gesterr

        msNomeChiave.Clear()
        msDescrizione.Clear()
        msStringaSql.Clear()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'valorizzo i campi presenti nella classe

    Private Sub ValorizzaCampi()

        On Error GoTo gesterr

        Dim lRiga As Long = mlRows - 1
        PulisciCampi()
        If lRiga < 0 Or lRiga > mDataSet.Tables(msTabella).Rows.Count - 1 Then
            Exit Sub
        End If
        msNomeChiave.Valore = cNullString(mDataSet.Tables(msTabella).Rows(lRiga).Item(msNomeChiave.DataField))
        msNomeChiave.Changed = False
        msDescrizione.Valore = cNullString(mDataSet.Tables(msTabella).Rows(lRiga).Item(msDescrizione.DataField))
        msDescrizione.Changed = False
        msStringaSql.Valore = cNullString(mDataSet.Tables(msTabella).Rows(lRiga).Item(msStringaSql.DataField))
        msStringaSql.Changed = False


        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'apro il dataset e valorizza le property della classe
    Private Sub ImpostaDataSet(ByVal sSQL As String)

        On Error GoTo gesterr

        Dim dbDataAdapter As New SqlClient.SqlDataAdapter(sSQL, mDBConn)
        mlRows = 1
        mDataSet.Clear()
        dbDataAdapter.Fill(mDataSet, msTabella)
        PulisciCampi()
        If mDataSet.Tables(msTabella).Rows.Count > 0 Then
            ValorizzaCampi()
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    ' leggo tutti i record della tabella
    Public Overloads Sub ReadAll()

        On Error GoTo gesterr

        sSQL = " Select "
        sSQL = sSQL & " * FROM " & msTabella & " "
        sSQL = sSQL & " ORDER BY " & msNomeChiave.DataField
        ImpostaDataSet(sSQL)

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'leggo tutti i record della tabella filtrando per codice
    Public Overloads Sub ReadAll(ByVal sCodice As String)

        On Error GoTo gesterr

        sSQL = " Select "
        sSQL = sSQL & " * FROM " & msTabella & " "
        sSQL = sSQL & " WHERE "
        sSQL = sSQL & msNomeChiave.DataField & " = " & GetSQLStr(sCodice)
        ImpostaDataSet(sSQL)

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    ' questa routine si crea da sola la stringa di update sul database
    ' per ogni campo scrivo la riga GetChangedUpdate(.......

    Private Function GetStringaUpdate() As String

        On Error GoTo gesterr

        Dim sStringa As String = ""
        Dim sVirgola As String = ""

        GetChangedUpdate(msNomeChiave, sStringa, sVirgola)
        GetChangedUpdate(msDescrizione, sStringa, sVirgola)
        GetChangedUpdate(msStringaSql, sStringa, sVirgola)

        Return sStringa

        Exit Function
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Function

    'Update del record selezionato
    Public Sub Update(ByVal sCodice As String)

        On Error GoTo gesterr

        Dim sStringaUpdate As String
        sStringaUpdate = GetStringaUpdate()
        If Len(Trim(sStringaUpdate)) = 0 Then Exit Sub

        sSQL = " UPDATE " & msTabella
        sSQL = sSQL & " SET "
        sSQL = sSQL & sStringaUpdate
        sSQL = sSQL & " WHERE "
        sSQL = sSQL & msNomeChiave.DataField & " = " & GetSQLStr(sCodice)
        Dim dbCommand As New SqlClient.SqlCommand(sSQL, mDBConn)
        dbCommand.ExecuteNonQuery()
        dbCommand = Nothing

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'Cancellazione del record selezionato
    Public Overloads Sub Delete(ByVal sCodice As String)

        On Error GoTo gesterr

        sSQL = " DELETE FROM " & msTabella
        sSQL = sSQL & " WHERE "
        sSQL = sSQL & msNomeChiave.DataField & " = " & GetSQLStr(sCodice)
        Dim dbCommand As New SqlClient.SqlCommand(sSQL, mDBConn)
        dbCommand.ExecuteNonQuery()
        dbCommand = Nothing

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'Inserimento di un nuovo record
    Public Sub Insert()

        On Error GoTo gesterr

        sSQL = " INSERT "
        sSQL = sSQL & " INTO " & msTabella
        sSQL = sSQL & " ( "
        sSQL = sSQL & msNomeChiave.DataField & ","
        sSQL = sSQL & msDescrizione.DataField & ","
        sSQL = sSQL & msStringaSql.DataField
        sSQL = sSQL & ")"
        sSQL = sSQL & " VALUES "
        sSQL = sSQL & " ( "
        sSQL = sSQL & msNomeChiave.ValoreSQL & ","
        sSQL = sSQL & msDescrizione.ValoreSQL & ","
        sSQL = sSQL & msStringaSql.ValoreSQL
        sSQL = sSQL & " ) "
        Dim dbCommand As New SqlClient.SqlCommand(sSQL, mDBConn)
        dbCommand.ExecuteNonQuery()
        dbCommand = Nothing

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'Questa routine mi restituisce la stringa sql da passare alla griglia di ricerca o di consultazione
    'faccio la like della stringa di ricerca su tutti i campi possibili
    Public Function GetGridViewSQL(ByVal sRicerca As String) As String

        On Error GoTo gesterr

        Dim sSQL As String = ""
        Dim sAnd As String = " WHERE "

        sSQL = " SELECT "
        sSQL = sSQL & " * "
        sSQL = sSQL & " FROM "
        sSQL = sSQL & msTabella

        If sRicerca.Trim.Length <> 0 Then
            'Filtro sia per codice che descrizione
            sSQL = sSQL & sAnd
            sSQL = sSQL & "("
            sSQL = sSQL & msNomeChiave.DataField & " Like " & GetSQLStr(sRicerca, , True)
            sSQL = sSQL & " OR "
            sSQL = sSQL & msDescrizione.DataField & " Like " & GetSQLStr(sRicerca, , True)
            sSQL = sSQL & " )"
            sAnd = " AND "
        End If

        sSQL = sSQL & " ORDER BY "
        sSQL = sSQL & msNomeChiave.DataField

        Return sSQL

        Exit Function
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Function

    'in questa routine imposto i nomi dei campi su database e la lunghezza massima che possono
    'raggiungere
    Public Sub New()

        On Error GoTo gesterr

        msNomeChiave.DataField = "nome_chiave"
        msNomeChiave.MaxLength = 50
        msDescrizione.DataField = "descrizione"
        msDescrizione.MaxLength = 250
        msStringaSql.DataField = "stringa_sql"

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

End Class
