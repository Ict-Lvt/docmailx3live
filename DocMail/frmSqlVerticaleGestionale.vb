Public Class frmSqlVerticaleGestionale

    Private mcSQLTsqlVerticaleGestionale As New clsTSqlVerticaleGestionale
    Private msRic As String
    Private msRic1 As String
    Private msStato As String
    Private msNomeForm As String = "frmSqlVerticaleGestionale"

    'imposto il maxlenght dei controlli solo per stringhe e date    
    Private Sub ImpostaDimensioneControlli()

        On Error GoTo gesterr

        txtNomeChiave.MaxLength = mcSQLTsqlVerticaleGestionale.NomeChiave.MaxLength
        txtDescrizione.MaxLength = mcSQLTsqlVerticaleGestionale.Descrizione.MaxLength

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub Carica()

        On Error GoTo gesterr

        'leggo il record per l'utente corrente
        'questa funzione mi valorizza i campi della classe mcSQLTAnaAliquote
        mcSQLTsqlVerticaleGestionale.ReadAll(msRic)

        If mcSQLTsqlVerticaleGestionale.RowsCount > 0 Then
            txtNomeChiave.Text = mcSQLTsqlVerticaleGestionale.NomeChiave.Valore
            txtDescrizione.Text = mcSQLTsqlVerticaleGestionale.Descrizione.Valore
            txtStringaSql.Text = mcSQLTsqlVerticaleGestionale.StringaSql.Valore
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub ImpostaStato()

        On Error GoTo gesterr

        'imposto lo stato del record e di conseguenza abilito/disabilito controlli e men�
        Select Case msStato

            Case "MODIFICA"
                EnableDisableControlli(True)
                tlbElimina.Enabled = True
            Case "NUOVO"
                EnableDisableControlli(True)
                SvuotaCampi()
                tlbElimina.Enabled = False
            Case Else

        End Select

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub ImpostaTabIndex()

        On Error GoTo gesterr

        Dim X As Integer

        'imposto l'ordine dei campi quando mi sposto premendo il pulsante TAB

        X = 0

        txtNomeChiave.TabIndex = X : X = X + 1
        txtDescrizione.TabIndex = X : X = X + 1
        txtStringaSql.TabIndex = X : X = X + 1

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub SvuotaCampi()

        On Error GoTo gesterr

        'svuoto i campi

        txtNomeChiave.Text = ""
        txtDescrizione.Text = ""
        txtStringaSql.Text = ""

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Function ControllaCampi() As Boolean

        On Error GoTo gesterr

        'controllo che i campi obbligatori siano valorizzati e che contengano dei dati validi

        If txtNomeChiave.Text.Trim.Length = 0 Then
            MsgBox("E' obbligatorio inserire il nome chiave", MsgBoxStyle.Information)
            Return False
            Exit Function
        End If

        If txtDescrizione.Text.Trim.Length = 0 Then
            MsgBox("E' obbligatorio inserire la descrizione", MsgBoxStyle.Information)
            Return False
            Exit Function
        End If

        'If txtStringaSql.Text.Trim.Length = 0 Then
        '    MsgBox("E' obbligatorio inserire una stringa sql", MsgBoxStyle.Information)
        '    Return False
        '    Exit Function
        'End If

        Return True

        Exit Function
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Function

    Private Sub Salva()

        On Error GoTo gesterr

        'controllo che i campi siano a posto
        If ControllaCampi() = False Then Exit Sub

        'passo i valori dei controlli dentro i campi di classe
        mcSQLTsqlVerticaleGestionale.NomeChiave.Valore = txtNomeChiave.Text
        mcSQLTsqlVerticaleGestionale.Descrizione.Valore = txtDescrizione.Text
        mcSQLTsqlVerticaleGestionale.StringaSql.Valore = txtStringaSql.Text

        'a seconda dello stato faccio una insert o un' update
        If msStato = "NUOVO" Then
            mcSQLTsqlVerticaleGestionale.Insert()
        Else
            mcSQLTsqlVerticaleGestionale.Update(mcSQLTsqlVerticaleGestionale.NomeChiave.Valore)
        End If

        'imposto la stato a nuovo dopo aver salvato i dati
        msStato = "NUOVO"
        ImpostaStato()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub frmSqlVerticaleGestionale_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMenu.Focus()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        On Error GoTo gesterr

        mcSQLTsqlVerticaleGestionale.OpenConnection()
        msStato = "NUOVO"
        ImpostaStato()
        EnableDisableControlli(True)
        ImpostaTabIndex()
        ImpostaDimensioneControlli()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub EnableDisableControlli(ByVal bAbilita As Boolean)

        On Error GoTo gesterr

        'se lo stato � modifica disabilito manualmente i campi che l'utente non pu� pi�
        'modificare, di solito i campi che compongono la chiave del record

        If msStato = "MODIFICA" And bAbilita = True Then
            txtNomeChiave.Enabled = False
        Else
            txtNomeChiave.Enabled = bAbilita
        End If

        txtDescrizione.Enabled = bAbilita
        txtStringaSql.Enabled = bAbilita

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub


    Private Sub tlbCerca_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tlbCerca.Click

        On Error GoTo gesterr

        Dim oFrm As New frmRicSqlVerticaleGestionale

        'apro il form di ricerca 
        oFrm.ShowDialog()

        'Imposto il focus per fars� che sia visibile il form menu di partenza
        frmMenu.Focus()
        Me.Focus()

        'se non ha selezionato niente esco
        If oFrm.msRic.Trim = "" Then Exit Sub : oFrm.Close()

        msStato = "MODIFICA"
        ImpostaStato()

        'assegno il valore chiave del record selezionato alla variabile locale di ricerca e
        'richiamo la procedura Carica che mi valorizza i dati a video

        msRic = oFrm.msRic
        oFrm.Close()
        Carica()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub tlbNuovo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tlbNuovo.Click
        On Error GoTo gesterr

        EnableDisableControlli(True)
        msStato = "NUOVO"
        ImpostaStato()
        txtNomeChiave.Focus()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)
    End Sub

    Private Sub tlbSalva_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tlbSalva.Click
        Salva()
    End Sub

    Private Sub tlbElimina_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tlbElimina.Click

        On Error GoTo gesterr

        Dim i As MsgBoxResult

        i = MsgBox("Sei sicuro di voler eliminare il record selezionato?", MsgBoxStyle.YesNo)

        If i = MsgBoxResult.No Then Exit Sub

        'dopo la conferma elimino il record selezionato ed imposto lo stato a nuovo
        'per un nuovo inserimento

        mcSQLTsqlVerticaleGestionale.Delete(mcSQLTsqlVerticaleGestionale.NomeChiave.Valore)

        msStato = "NUOVO"
        ImpostaStato()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub tlbEsci_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tlbEsci.Click

        On Error GoTo gesterr

        Me.Close()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

End Class