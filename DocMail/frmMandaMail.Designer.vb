<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMandaMail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tlsUtenti = New System.Windows.Forms.ToolStrip
        Me.tlbAggiorna = New System.Windows.Forms.ToolStripButton
        Me.tlbInvia = New System.Windows.Forms.ToolStripButton
        Me.tlbSelTutto = New System.Windows.Forms.ToolStripButton
        Me.tlbEsci = New System.Windows.Forms.ToolStripButton
        Me.grdFatture = New System.Windows.Forms.DataGridView
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.nome_file = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cod_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.desc_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.mail_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.note = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.stato = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Aggiungi = New System.Windows.Forms.DataGridViewImageColumn
        Me.lblDescrizione = New System.Windows.Forms.Label
        Me.prg = New System.Windows.Forms.ProgressBar
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.grdFattureInvio = New System.Windows.Forms.DataGridView
        Me.idinvio = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Togli = New System.Windows.Forms.DataGridViewImageColumn
        Me.nomefile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.codcliente = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.desccliente = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.emailcliente = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.notecliente = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.statoinvio = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewImageColumn1 = New System.Windows.Forms.DataGridViewImageColumn
        Me.DataGridViewImageColumn2 = New System.Windows.Forms.DataGridViewImageColumn
        Me.btnSfoglia = New System.Windows.Forms.Button
        Me.txtAllegato = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.ofdFile = New System.Windows.Forms.OpenFileDialog
        Me.tlsUtenti.SuspendLayout()
        CType(Me.grdFatture, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdFattureInvio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsUtenti
        '
        Me.tlsUtenti.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.tlsUtenti.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tlbAggiorna, Me.tlbInvia, Me.tlbSelTutto, Me.tlbEsci})
        Me.tlsUtenti.Location = New System.Drawing.Point(0, 0)
        Me.tlsUtenti.Name = "tlsUtenti"
        Me.tlsUtenti.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tlsUtenti.Size = New System.Drawing.Size(1173, 39)
        Me.tlsUtenti.Stretch = True
        Me.tlsUtenti.TabIndex = 68
        Me.tlsUtenti.Text = "ToolStrip1"
        '
        'tlbAggiorna
        '
        Me.tlbAggiorna.Image = Global.DocMailX3.My.Resources.Resources.Windows_View_Detail
        Me.tlbAggiorna.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbAggiorna.Name = "tlbAggiorna"
        Me.tlbAggiorna.Size = New System.Drawing.Size(195, 36)
        Me.tlbAggiorna.Text = "Aggiorna Contenuto Cartella"
        '
        'tlbInvia
        '
        Me.tlbInvia.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlbInvia.Image = Global.DocMailX3.My.Resources.Resources.CRISTAL_MAIL
        Me.tlbInvia.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbInvia.Name = "tlbInvia"
        Me.tlbInvia.Size = New System.Drawing.Size(101, 36)
        Me.tlbInvia.Text = "Invia Mail"
        '
        'tlbSelTutto
        '
        Me.tlbSelTutto.Image = Global.DocMailX3.My.Resources.Resources._2rightarrow
        Me.tlbSelTutto.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbSelTutto.Name = "tlbSelTutto"
        Me.tlbSelTutto.Size = New System.Drawing.Size(193, 36)
        Me.tlbSelTutto.Text = "Metti tutti i file in spedizione"
        '
        'tlbEsci
        '
        Me.tlbEsci.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlbEsci.Image = Global.DocMailX3.My.Resources.Resources.Run44
        Me.tlbEsci.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbEsci.Name = "tlbEsci"
        Me.tlbEsci.Size = New System.Drawing.Size(66, 36)
        Me.tlbEsci.Text = "Esci"
        '
        'grdFatture
        '
        Me.grdFatture.AllowUserToAddRows = False
        Me.grdFatture.AllowUserToDeleteRows = False
        Me.grdFatture.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdFatture.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdFatture.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.nome_file, Me.cod_cliente, Me.desc_cliente, Me.mail_cliente, Me.note, Me.stato, Me.Aggiungi})
        Me.grdFatture.Location = New System.Drawing.Point(14, 126)
        Me.grdFatture.Name = "grdFatture"
        Me.grdFatture.Size = New System.Drawing.Size(572, 359)
        Me.grdFatture.TabIndex = 69
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.Visible = False
        '
        'nome_file
        '
        Me.nome_file.DataPropertyName = "nome_file"
        Me.nome_file.HeaderText = "Nome File"
        Me.nome_file.Name = "nome_file"
        '
        'cod_cliente
        '
        Me.cod_cliente.DataPropertyName = "cod_cliente"
        Me.cod_cliente.HeaderText = "Cod. Cliente"
        Me.cod_cliente.Name = "cod_cliente"
        Me.cod_cliente.Width = 50
        '
        'desc_cliente
        '
        Me.desc_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.desc_cliente.DataPropertyName = "desc_cliente"
        Me.desc_cliente.HeaderText = "Desc. Cliente"
        Me.desc_cliente.Name = "desc_cliente"
        '
        'mail_cliente
        '
        Me.mail_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.mail_cliente.DataPropertyName = "mail_cliente"
        Me.mail_cliente.HeaderText = "Email"
        Me.mail_cliente.Name = "mail_cliente"
        '
        'note
        '
        Me.note.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.note.DataPropertyName = "note"
        Me.note.HeaderText = "Note"
        Me.note.Name = "note"
        '
        'stato
        '
        Me.stato.DataPropertyName = "stato"
        Me.stato.HeaderText = "Stato"
        Me.stato.Name = "stato"
        Me.stato.Visible = False
        '
        'Aggiungi
        '
        Me.Aggiungi.HeaderText = "+"
        Me.Aggiungi.Image = Global.DocMailX3.My.Resources.Resources._2rightarrow
        Me.Aggiungi.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.Aggiungi.Name = "Aggiungi"
        Me.Aggiungi.Width = 40
        '
        'lblDescrizione
        '
        Me.lblDescrizione.AutoSize = True
        Me.lblDescrizione.Location = New System.Drawing.Point(14, 48)
        Me.lblDescrizione.Name = "lblDescrizione"
        Me.lblDescrizione.Size = New System.Drawing.Size(146, 13)
        Me.lblDescrizione.TabIndex = 71
        Me.lblDescrizione.Text = "Lettura e conversione del file "
        Me.lblDescrizione.Visible = False
        '
        'prg
        '
        Me.prg.Location = New System.Drawing.Point(362, 46)
        Me.prg.Name = "prg"
        Me.prg.Size = New System.Drawing.Size(620, 15)
        Me.prg.TabIndex = 72
        Me.prg.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 110)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(308, 13)
        Me.Label1.TabIndex = 75
        Me.Label1.Text = "FILE PRESENTI NELLA CARTELLA DA ELABORARE"
        Me.Label1.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(589, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 13)
        Me.Label2.TabIndex = 76
        Me.Label2.Text = "FILE DA SPEDIRE"
        Me.Label2.Visible = False
        '
        'grdFattureInvio
        '
        Me.grdFattureInvio.AllowUserToAddRows = False
        Me.grdFattureInvio.AllowUserToDeleteRows = False
        Me.grdFattureInvio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdFattureInvio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdFattureInvio.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idinvio, Me.Togli, Me.nomefile, Me.codcliente, Me.desccliente, Me.emailcliente, Me.notecliente, Me.statoinvio})
        Me.grdFattureInvio.Location = New System.Drawing.Point(592, 126)
        Me.grdFattureInvio.Name = "grdFattureInvio"
        Me.grdFattureInvio.Size = New System.Drawing.Size(572, 359)
        Me.grdFattureInvio.TabIndex = 77
        '
        'idinvio
        '
        Me.idinvio.DataPropertyName = "id"
        Me.idinvio.HeaderText = "id"
        Me.idinvio.Name = "idinvio"
        Me.idinvio.Visible = False
        '
        'Togli
        '
        Me.Togli.HeaderText = "-"
        Me.Togli.Image = Global.DocMailX3.My.Resources.Resources._2leftarrowSmall
        Me.Togli.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.Togli.Name = "Togli"
        Me.Togli.Width = 40
        '
        'nomefile
        '
        Me.nomefile.DataPropertyName = "nome_file"
        Me.nomefile.HeaderText = "Nome File"
        Me.nomefile.Name = "nomefile"
        '
        'codcliente
        '
        Me.codcliente.DataPropertyName = "cod_cliente"
        Me.codcliente.HeaderText = "Cod. Cliente"
        Me.codcliente.Name = "codcliente"
        Me.codcliente.Width = 50
        '
        'desccliente
        '
        Me.desccliente.DataPropertyName = "desc_cliente"
        Me.desccliente.HeaderText = "Desc. Cliente"
        Me.desccliente.Name = "desccliente"
        Me.desccliente.Width = 130
        '
        'emailcliente
        '
        Me.emailcliente.DataPropertyName = "mail_cliente"
        Me.emailcliente.HeaderText = "Email"
        Me.emailcliente.Name = "emailcliente"
        '
        'notecliente
        '
        Me.notecliente.DataPropertyName = "note"
        Me.notecliente.HeaderText = "Note"
        Me.notecliente.Name = "notecliente"
        '
        'statoinvio
        '
        Me.statoinvio.DataPropertyName = "stato"
        Me.statoinvio.HeaderText = "Stato"
        Me.statoinvio.Name = "statoinvio"
        Me.statoinvio.Visible = False
        '
        'DataGridViewImageColumn1
        '
        Me.DataGridViewImageColumn1.HeaderText = "+"
        Me.DataGridViewImageColumn1.Image = Global.DocMailX3.My.Resources.Resources._2rightarrow
        Me.DataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.DataGridViewImageColumn1.Name = "DataGridViewImageColumn1"
        Me.DataGridViewImageColumn1.Width = 40
        '
        'DataGridViewImageColumn2
        '
        Me.DataGridViewImageColumn2.HeaderText = "-"
        Me.DataGridViewImageColumn2.Image = Global.DocMailX3.My.Resources.Resources._2leftarrowSmall
        Me.DataGridViewImageColumn2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.DataGridViewImageColumn2.Name = "DataGridViewImageColumn2"
        Me.DataGridViewImageColumn2.Width = 40
        '
        'btnSfoglia
        '
        Me.btnSfoglia.Location = New System.Drawing.Point(160, 74)
        Me.btnSfoglia.Name = "btnSfoglia"
        Me.btnSfoglia.Size = New System.Drawing.Size(31, 23)
        Me.btnSfoglia.TabIndex = 83
        Me.btnSfoglia.Text = "..."
        Me.btnSfoglia.UseVisualStyleBackColor = True
        '
        'txtAllegato
        '
        Me.txtAllegato.Enabled = False
        Me.txtAllegato.Location = New System.Drawing.Point(197, 75)
        Me.txtAllegato.Name = "txtAllegato"
        Me.txtAllegato.Size = New System.Drawing.Size(785, 20)
        Me.txtAllegato.TabIndex = 82
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(14, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 13)
        Me.Label3.TabIndex = 81
        Me.Label3.Text = "Allegato aggiuntivo"
        '
        'frmMandaMail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1173, 498)
        Me.Controls.Add(Me.btnSfoglia)
        Me.Controls.Add(Me.txtAllegato)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.grdFattureInvio)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.prg)
        Me.Controls.Add(Me.lblDescrizione)
        Me.Controls.Add(Me.grdFatture)
        Me.Controls.Add(Me.tlsUtenti)
        Me.Name = "frmMandaMail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestione Invio Documenti"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tlsUtenti.ResumeLayout(False)
        Me.tlsUtenti.PerformLayout()
        CType(Me.grdFatture, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdFattureInvio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tlsUtenti As System.Windows.Forms.ToolStrip
    Friend WithEvents tlbInvia As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlbEsci As System.Windows.Forms.ToolStripButton
    Friend WithEvents grdFatture As System.Windows.Forms.DataGridView
    Friend WithEvents lblDescrizione As System.Windows.Forms.Label
    Friend WithEvents prg As System.Windows.Forms.ProgressBar
    Friend WithEvents tlbAggiorna As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grdFattureInvio As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewImageColumn1 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents DataGridViewImageColumn2 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nome_file As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cod_cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents desc_cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents mail_cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents note As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Aggiungi As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents tlbSelTutto As System.Windows.Forms.ToolStripButton
    Friend WithEvents idinvio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Togli As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents nomefile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents codcliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents desccliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents emailcliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents notecliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents statoinvio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSfoglia As System.Windows.Forms.Button
    Friend WithEvents txtAllegato As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ofdFile As System.Windows.Forms.OpenFileDialog
End Class
