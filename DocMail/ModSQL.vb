Imports System
Imports System.IO


Module ModSQL
    'Provider=SQLOLEDB.1;
    Public g_sConnStringTecno = GetStringaConnessione()

    Public Function GetDatKey() As String
        Dim sValore As String
        sValore = Now.Year & Now.Month & Now.Day & Now.Hour & Now.Minute & Now.Second & Now.Millisecond
        Return sValore
    End Function

    Public Function GetStringaConnessione() As String

        On Error GoTo gesterr

        Dim i As Long
        Dim str As String

        i = FreeFile()
        Microsoft.VisualBasic.FileOpen(i, "DocMail.ini", OpenMode.Input)
        str = LineInput(i)
        FileClose(i)
        Return str
        
        Exit Function
gesterr:
        FileClose(i)
        MsgBox(Err.Number & " " & Err.Description)

    End Function

    Public Function GetSQLStr(ByVal sValore As String, Optional ByVal bNoApici As Boolean = False, Optional ByVal bLike As Boolean = False) As String
        Dim sRisultato As String = ""
        sRisultato = RTrim(sValore)
        If bNoApici Then
            sRisultato = Replace(sRisultato, "'", "''")
        Else
            If bLike = True Then
                sRisultato = "'%" & Replace(sRisultato, "'", "''") & "%'"
            Else
                sRisultato = "'" & Replace(sRisultato, "'", "''") & "'"
            End If

        End If
        Return sRisultato

    End Function

    Public Function GetSQLFloat(ByVal sValore As String) As String
        Dim sRisultato As String
        sRisultato = Replace(sValore, ",", ".")
        Return sRisultato
    End Function

    Public Function GetSQLDate(ByVal sValore As String) As String
        Dim sRisultato As String
        If Len(Trim(sValore)) = 0 Or IsDate(sValore) = False Then
            sValore = "01/01/1900"
        End If
        sRisultato = "CONVERT(DATETIME, '" & Format(CDate(sValore), "yyyy-MM-dd") & " 00:00:00', 102)"
        Return sRisultato
    End Function

    Public Function GetSQLTime(ByVal sValore As Date) As String
        Dim sRisultato As String
        sRisultato = "CONVERT(DATETIME, '1900-01-01 " & Format(sValore, "HH:mm:ss") & "', 102)"
        Return Replace(sRisultato, ".", ":")
    End Function

    Public Function GetSQLDateRisc(ByVal sValore As String) As String
        Dim sRisultato As String
        If Len(Trim(sValore)) = 0 Or IsDate(sValore) = False Then
            sValore = "01/01/1900"
        End If
        sRisultato = GetSQLStr(Format(CDate(sValore), "MM/dd/yyyy"))
        Return sRisultato
    End Function

    Public Function cNullString(ByVal oCampo As Object) As String
        Dim sValore As String = ""
        If IsDBNull(oCampo) Then
            sValore = ""
        Else
            sValore = oCampo
        End If
        Return sValore.Trim
    End Function

    Public Function cNullLong(ByVal oCampo As Object) As Long
        Dim sValore As Long = 0
        If IsDBNull(oCampo) Then
            sValore = 0
        Else
            sValore = oCampo
        End If
        Return sValore
    End Function

    Public Function cNullDouble(ByVal oCampo As Object) As Double
        Dim sValore As Double = 0
        If IsDBNull(oCampo) Then
            sValore = 0
        Else
            sValore = oCampo
        End If
        Return sValore
    End Function

    Public Function cNullDate(ByVal oCampo As Object) As Date
        Dim sValore As Date
        If IsDBNull(oCampo) Then
            sValore = "01/01/1900"
        Else
            If IsDate(oCampo) = False Then
                sValore = "01/01/1900"
            Else
                sValore = oCampo
            End If
        End If
        Return sValore
    End Function

    Public Function GetMaxDim(ByVal sValore As String, ByVal lMaxLen As Long) As String
        sValore = RTrim(sValore)
        If lMaxLen < 1 Then
            Return ""
        End If
        If Len(sValore) > lMaxLen Then
            Return Mid(sValore, 1, lMaxLen)
        Else
            Return sValore
        End If
    End Function

    Public Sub GetChangedUpdate(ByRef Oggetto As Object, ByRef sStringa As String, ByRef sVirgola As String)
        If Oggetto.Changed Then
            sStringa = sStringa & sVirgola & Oggetto.DataField & " = " & Oggetto.ValoreSQL
            sVirgola = ","
        End If

    End Sub

    Public Function GetStringaCombo(ByVal sVal As String, ByVal lLen As Long, ByVal sVal2 As String, Optional ByVal sSeparatore As String = ".") As String

        Dim str As String
        Dim sSep As New String(sSeparatore, lLen)

        str = Left(sVal & sSep, lLen) & sVal2
        Return str

    End Function

End Module
