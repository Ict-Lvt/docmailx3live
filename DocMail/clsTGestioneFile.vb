Public Class clsTGestioneFile

    Private msTabella As String = "TGestioneFile"
    Private msNomeForm As String = "clsTGestioneFile"
    Public mDBConn As New SqlClient.SqlConnection(g_sConnStringTecno)
    Private mDataSet As New DataSet("DS" & msTabella)
    Private sSQL As String
    Private mlRows As Long

    ' per ogni campo della tabella dichiaro una variabile
    Private mlId As New clsFieldLong
    Private msNomeFile As New clsFieldString '250
    Private msCodCliente As New clsFieldString '50
    Private msDescCliente As New clsFieldString '250
    Private msMailCliente As New clsFieldString '250
    Private msNote As New clsFieldString '250
    Private msStato As New clsFieldString '1

    Private msStringaConnessione As String

    ' ... e scrivo le property get/set

    Public Property Id() As clsFieldLong
        Get
            Return mlId
        End Get
        Set(ByVal value As clsFieldLong)
            '    mlId = value
        End Set
    End Property

    Public Property NomeFile() As clsFieldString
        Get
            Return msNomeFile
        End Get
        Set(ByVal value As clsFieldString)
            msNomeFile = value
        End Set
    End Property

    Public Property CodCliente() As clsFieldString
        Get
            Return msCodCliente
        End Get
        Set(ByVal value As clsFieldString)
            msCodCliente = value
        End Set
    End Property

    Public Property DescCliente() As clsFieldString
        Get
            Return msDescCliente
        End Get
        Set(ByVal value As clsFieldString)
            msDescCliente = value
        End Set
    End Property

    Public Property MailCliente() As clsFieldString
        Get
            Return msMailCliente
        End Get
        Set(ByVal value As clsFieldString)
            msMailCliente = value
        End Set
    End Property

    Public Property Note() As clsFieldString
        Get
            Return msNote
        End Get
        Set(ByVal value As clsFieldString)
            msNote = value
        End Set
    End Property

    Public Property Stato() As clsFieldString
        Get
            Return msStato
        End Get
        Set(ByVal value As clsFieldString)
            msStato = value
        End Set
    End Property

    'apro la connessione al db
    Public Sub OpenConnection()

        On Error GoTo gesterr

        If mDBConn.State = ConnectionState.Closed Then
            mDBConn.Open()
        End If

        msStringaConnessione = g_sConnStringTecno

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'chiudo la connessione al db
    Public Sub CloseConnection()

        On Error GoTo gesterr

        mDBConn.Close()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Public Property Rows() As Long
        Get
            Return mlRows
        End Get
        Set(ByVal value As Long)
            If mlRows < 1 Then mlRows = 1
            mlRows = value
            ValorizzaCampi()
        End Set
    End Property

    ' mi restituisce quante righe ha trovato nel dataset
    Public ReadOnly Property RowsCount() As Long
        Get
            Return mDataSet.Tables(msTabella).Rows.Count
        End Get
    End Property

    'svuoto i campi
    Private Sub PulisciCampi()

        On Error GoTo gesterr

        mlId.Clear()
        msNomeFile.Clear()
        msCodCliente.Clear()
        msDescCliente.Clear()
        msMailCliente.Clear()
        msNote.Clear()
        msStato.Clear()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'valorizzo i campi presenti nella classe

    Private Sub ValorizzaCampi()

        On Error GoTo gesterr

        Dim lRiga As Long = mlRows - 1
        PulisciCampi()
        If lRiga < 0 Or lRiga > mDataSet.Tables(msTabella).Rows.Count - 1 Then
            Exit Sub
        End If

        mlId.Valore = cNullLong(mDataSet.Tables(msTabella).Rows(lRiga).Item(mlId.DataField))
        mlId.Changed = False
        msNomeFile.Valore = cNullString(mDataSet.Tables(msTabella).Rows(lRiga).Item(msNomeFile.DataField))
        msNomeFile.Changed = False
        msCodCliente.Valore = cNullString(mDataSet.Tables(msTabella).Rows(lRiga).Item(msCodCliente.DataField))
        msCodCliente.Changed = False
        msDescCliente.Valore = cNullString(mDataSet.Tables(msTabella).Rows(lRiga).Item(msDescCliente.DataField))
        msDescCliente.Changed = False
        msMailCliente.Valore = cNullString(mDataSet.Tables(msTabella).Rows(lRiga).Item(msMailCliente.DataField))
        msMailCliente.Changed = False
        msNote.Valore = cNullString(mDataSet.Tables(msTabella).Rows(lRiga).Item(msNote.DataField))
        msNote.Changed = False
        msStato.Valore = cNullString(mDataSet.Tables(msTabella).Rows(lRiga).Item(msStato.DataField))
        msStato.Changed = False

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'apro il dataset e valorizza le property della classe
    Private Sub ImpostaDataSet(ByVal sSQL As String)

        On Error GoTo gesterr

        Dim dbDataAdapter As New SqlClient.SqlDataAdapter(sSQL, mDBConn)
        mlRows = 1
        mDataSet.Clear()
        dbDataAdapter.Fill(mDataSet, msTabella)
        PulisciCampi()
        If mDataSet.Tables(msTabella).Rows.Count > 0 Then
            ValorizzaCampi()
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    ' leggo tutti i record della tabella
    Public Overloads Sub ReadAll()

        On Error GoTo gesterr

        sSQL = " Select "
        sSQL = sSQL & " * FROM " & msTabella & " "
        sSQL = sSQL & " ORDER BY " & msNomeFile.DataField
        ImpostaDataSet(sSQL)

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'leggo tutti i record della tabella filtrando per codice
    Public Overloads Sub ReadAll(ByVal lId As Long)

        On Error GoTo gesterr

        sSQL = " Select "
        sSQL = sSQL & " * FROM " & msTabella & " "
        sSQL = sSQL & " WHERE "
        sSQL = sSQL & mlId.DataField & " = " & GetSQLFloat(lId)
        ImpostaDataSet(sSQL)

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'leggo tutti i record della tabella filtrando per codice
    Public Overloads Sub ReadAll(ByVal sStato As String)

        On Error GoTo gesterr

        sSQL = " Select "
        sSQL = sSQL & " * FROM " & msTabella & " "
        sSQL = sSQL & " WHERE "
        sSQL = sSQL & msStato.DataField & " = " & GetSQLStr(sStato)
        ImpostaDataSet(sSQL)

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    ' questa routine si crea da sola la stringa di update sul database
    ' per ogni campo scrivo la riga GetChangedUpdate(.......

    Private Function GetStringaUpdate() As String

        On Error GoTo gesterr

        Dim sStringa As String = ""
        Dim sVirgola As String = ""

        GetChangedUpdate(msNomeFile, sStringa, sVirgola)
        GetChangedUpdate(msCodCliente, sStringa, sVirgola)
        GetChangedUpdate(msDescCliente, sStringa, sVirgola)
        GetChangedUpdate(msMailCliente, sStringa, sVirgola)
        GetChangedUpdate(msNote, sStringa, sVirgola)
        GetChangedUpdate(msStato, sStringa, sVirgola)

        Return sStringa

        Exit Function
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Function

    'Update del record selezionato
    Public Sub Update(ByVal lId As Long)

        On Error GoTo gesterr

        Dim sStringaUpdate As String
        sStringaUpdate = GetStringaUpdate()
        If Len(Trim(sStringaUpdate)) = 0 Then Exit Sub

        sSQL = " UPDATE " & msTabella
        sSQL = sSQL & " SET "
        sSQL = sSQL & sStringaUpdate
        sSQL = sSQL & " WHERE "
        sSQL = sSQL & mlId.DataField & " = " & GetSQLFloat(lId)
        Dim dbCommand As New SqlClient.SqlCommand(sSQL, mDBConn)
        dbCommand.ExecuteNonQuery()
        dbCommand = Nothing

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Public Sub UpdateAllSpedizione()

        On Error GoTo gesterr

        Dim sStringaUpdate As String
        sStringaUpdate = GetStringaUpdate()
        If Len(Trim(sStringaUpdate)) = 0 Then Exit Sub

        sSQL = " UPDATE " & msTabella
        sSQL = sSQL & " SET " & msStato.DataField & " = 'D' where " & msStato.DataField & " = 'C'"
        Dim dbCommand As New SqlClient.SqlCommand(sSQL, mDBConn)
        dbCommand.ExecuteNonQuery()
        dbCommand = Nothing

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'Cancellazione del record selezionato
    Public Overloads Sub Delete(ByVal lId As Long)

        On Error GoTo gesterr

        sSQL = " DELETE FROM " & msTabella
        sSQL = sSQL & " WHERE "
        sSQL = sSQL & mlId.DataField & " = " & GetSQLFloat(lId)
        Dim dbCommand As New SqlClient.SqlCommand(sSQL, mDBConn)
        dbCommand.ExecuteNonQuery()
        dbCommand = Nothing

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'Cancellazione del record selezionato
    Public Overloads Sub Delete(ByVal sStato As String)

        On Error GoTo gesterr

        sSQL = " DELETE FROM " & msTabella
        sSQL = sSQL & " WHERE "
        sSQL = sSQL & msStato.DataField & " = " & GetSQLStr(sStato)
        Dim dbCommand As New SqlClient.SqlCommand(sSQL, mDBConn)
        dbCommand.ExecuteNonQuery()
        dbCommand = Nothing

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'Inserimento di un nuovo record
    Public Sub Insert()

        On Error GoTo gesterr

        sSQL = " INSERT "
        sSQL = sSQL & " INTO " & msTabella
        sSQL = sSQL & " ( "
        sSQL = sSQL & msNomeFile.DataField & ","
        sSQL = sSQL & msCodCliente.DataField & ","
        sSQL = sSQL & msDescCliente.DataField & ","
        sSQL = sSQL & msMailCliente.DataField & ","
        sSQL = sSQL & msNote.DataField & ","
        sSQL = sSQL & msStato.DataField
        sSQL = sSQL & ")"
        sSQL = sSQL & " VALUES "
        sSQL = sSQL & " ( "
        sSQL = sSQL & msNomeFile.ValoreSQL & ","
        sSQL = sSQL & msCodCliente.ValoreSQL & ","
        sSQL = sSQL & msDescCliente.ValoreSQL & ","
        sSQL = sSQL & msMailCliente.ValoreSQL & ","
        sSQL = sSQL & msNote.ValoreSQL & ","
        sSQL = sSQL & msStato.ValoreSQL
        sSQL = sSQL & " ) "
        Dim dbCommand As New SqlClient.SqlCommand(sSQL, mDBConn)
        dbCommand.ExecuteNonQuery()
        dbCommand = Nothing

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    'Questa routine mi restituisce la stringa sql da passare alla griglia di ricerca o di consultazione
    'faccio la like della stringa di ricerca su tutti i campi possibili
    Public Function GetGridViewSQL(ByVal sRicerca As String) As String

        On Error GoTo gesterr

        Dim sSQL As String = ""
        Dim sAnd As String = " WHERE "

        sSQL = " SELECT "
        sSQL = sSQL & " * "
        sSQL = sSQL & " FROM "
        sSQL = sSQL & msTabella

        If sRicerca.Trim.Length <> 0 Then
            'Filtro sia per codice che descrizione
            sSQL = sSQL & sAnd
            sSQL = sSQL & msStato.DataField & " = " & GetSQLStr(sRicerca)
            sAnd = " AND "
        End If

        sSQL = sSQL & " ORDER BY "
        sSQL = sSQL & msNomeFile.DataField

        Return sSQL

        Exit Function
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Function

    'in questa routine imposto i nomi dei campi su database e la lunghezza massima che possono
    'raggiungere
    Public Sub New()

        On Error GoTo gesterr

        mlId.DataField = "id"
        msNomeFile.DataField = "nome_file"
        msNomeFile.MaxLength = 50
        msCodCliente.DataField = "cod_cliente"
        msCodCliente.MaxLength = 50
        msDescCliente.DataField = "desc_cliente"
        msDescCliente.MaxLength = 250
        msMailCliente.DataField = "mail_cliente"
        msMailCliente.MaxLength = 250
        msNote.DataField = "note"
        msNote.MaxLength = 250
        msStato.DataField = "stato"
        msStato.MaxLength = 1

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

End Class
