﻿Imports System.IO
Imports System.Reflection
Imports SevenZip

Public Class ZipCompressor
    Private Const Dll7z32 As String = "7z(x32).dll"
    Private Const Dll7z64 As String = "7z(x64).dll"

    Public Sub Compress(source As String, destArchive As String)
        Dim assembly As Assembly = Assembly.GetExecutingAssembly()
        Dim runFolder As String = Path.GetDirectoryName(assembly.Location)
        Dim fileCompress As String = Path.Combine(Path.GetDirectoryName(destArchive), Path.GetFileNameWithoutExtension(destArchive) & ".zip")

        If Environment.Is64BitProcess Then
            SevenZipCompressor.SetLibraryPath(runFolder & "\7Zip\" & Dll7z64)
        Else
            SevenZipCompressor.SetLibraryPath(runFolder & "\7Zip\" & Dll7z32)
        End If

        Dim szc As SevenZipCompressor = New SevenZipCompressor()
        szc.ArchiveFormat = OutArchiveFormat.Zip

        If File.Exists(fileCompress) Then
            File.Delete(fileCompress)
        End If

        If Not File.Exists(fileCompress) Then
            szc.CompressDirectory(source, fileCompress)
        End If
    End Sub
End Class
