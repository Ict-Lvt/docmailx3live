Public Class frmMandaMail

    Private msNomeForm As String = "frmMandaMail"
    Private mcTGestioneFile As New clsTGestioneFile
    Private mcTSqlVerticaleGestionale As New clsTSqlVerticaleGestionale

    Private Sub frmMandaMail_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        On Error GoTo gesterr

        mcTGestioneFile.CloseConnection()
        mcTSqlVerticaleGestionale.CloseConnection()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub frmMandaMail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        On Error GoTo gesterr

        mcTGestioneFile.OpenConnection()
        mcTSqlVerticaleGestionale.OpenConnection()

        'AggiornaElencoCartelle()
        'AggiornaGriglie()

        mcTGestioneFile.Delete("C")
        mcTGestioneFile.Delete("D")

        Me.Focus()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub AggiornaElencoCartelle()

        On Error GoTo gesterr

        Dim j As Long
        Dim k As Long
        Dim i As Long
        Dim lFineGriglia As Long
        Dim lNrFile As Long
        Dim sCartella As String
        Dim sCartella1 As String
        Dim sFile As String
        Dim sCodCliente As String
        Dim lLunghezzaCodice As Long
        Dim lInizioCodice As Long

        mcTGestioneFile.Delete("C")
        mcTGestioneFile.Delete("D")
        mcTSqlVerticaleGestionale.ReadAll("CARTELLAFILE")

        If mcTSqlVerticaleGestionale.RowsCount = 0 Then
            Exit Sub
        Else
            sCartella = mcTSqlVerticaleGestionale.StringaSql.Valore & "*.pdf"
            sCartella1 = mcTSqlVerticaleGestionale.StringaSql.Valore
        End If

        mcTSqlVerticaleGestionale.ReadAll("LUNGHEZZACODCLIENTE")
        lLunghezzaCodice = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("INIZIOCODICE")
        lInizioCodice = mcTSqlVerticaleGestionale.StringaSql.Valore

        sFile = Dir(sCartella)

        While sFile <> ""
            lNrFile = lNrFile + 1
            sFile = Dir()
        End While

        prg.Minimum = 0
        prg.Maximum = lNrFile
        prg.Visible = True
        lblDescrizione.Visible = True

        sFile = Dir(sCartella)

        For i = 0 To lNrFile - 1

            lblDescrizione.Text = "Lettura e conversione del file " & sFile & " in corso ..."
            prg.Value = i + 1
            Me.Refresh()

            'Shell(sCartella1 & "minetext.exe " & sCartella1 & sFile & " " & sCartella1 & Replace(sFile, "pdf", "txt"), , True)

            sCodCliente = Mid(sFile, lInizioCodice, lLunghezzaCodice) 'LeggiCodCliente(CStr(sCartella1 & Replace(sFile, "pdf", "txt")))
            'LeggiEdAggiornaCliente(sCodCliente, Replace(sFile, "pdf", "txt"))
            LeggiEdAggiornaCliente(sCodCliente, sFile)

            sFile = Dir()

        Next

        AggiornaGriglie()

        prg.Visible = False
        lblDescrizione.Visible = False

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub LeggiEdAggiornaCliente(ByVal sCodCliente As String, ByVal sNomeFile As String)

        On Error GoTo gesterr

        Dim sSqlConn As String = ""
        Dim sSql As String = ""
        Dim DataGest As DataSet = New DataSet("DataGest")
        Dim AdapGest As OleDb.OleDbDataAdapter
        Dim DBConnComm As OleDb.OleDbConnection

        'Leggo Stringa di connessione
        mcTSqlVerticaleGestionale.ReadAll("CONN")

        If mcTSqlVerticaleGestionale.RowsCount() > 0 Then
            sSqlConn = mcTSqlVerticaleGestionale.StringaSql.Valore
            DBConnComm = New OleDb.OleDbConnection(sSqlConn)
            DBConnComm.Open()

            mcTGestioneFile.CodCliente.Valore = sCodCliente
            mcTGestioneFile.Stato.Valore = "C"
            mcTGestioneFile.NomeFile.Valore = sNomeFile

            mcTSqlVerticaleGestionale.ReadAll("RMAILCLIENTE")

            If mcTSqlVerticaleGestionale.RowsCount() > 0 Then
                sSql = mcTSqlVerticaleGestionale.StringaSql.Valore
                sSql = Replace(sSql, "@CodCliente", sCodCliente)

                AdapGest = New OleDb.OleDbDataAdapter(sSql, DBConnComm)
                AdapGest.Fill(DataGest, "AdapGest")

                If DataGest.Tables("AdapGest").Rows.Count = 0 Then
                    mcTGestioneFile.MailCliente.Valore = ""
                    mcTGestioneFile.DescCliente.Valore = ""
                    mcTGestioneFile.Note.Valore = "Codice Cliente non presente sul gestionale"
                Else
                    mcTGestioneFile.MailCliente.Valore = DataGest.Tables("AdapGest").Rows(0).Item("email_cliente") & ""
                    'mcTGestioneFile.DescCliente.Valore = DataGest.Tables("AdapGest").Rows(0).Item("desc_cliente")
                    mcTGestioneFile.Note.Valore = ""
                    If mcTGestioneFile.MailCliente.Valore = "" Then mcTGestioneFile.Note.Valore = "Indirizzo email non presente per il cliente"
                End If

                AdapGest = Nothing
                DataGest.Clear()

                mcTSqlVerticaleGestionale.ReadAll("RDESCCLIENTE")

                If mcTSqlVerticaleGestionale.RowsCount() > 0 Then
                    sSql = mcTSqlVerticaleGestionale.StringaSql.Valore
                    sSql = Replace(sSql, "@CodCliente", sCodCliente)

                    AdapGest = New OleDb.OleDbDataAdapter(sSql, DBConnComm)
                    AdapGest.Fill(DataGest, "AdapGest")

                    If DataGest.Tables("AdapGest").Rows.Count = 0 Then
                        mcTGestioneFile.DescCliente.Valore = ""
                        mcTGestioneFile.Note.Valore = "Codice Cliente non presente sul gestionale"
                    Else
                        mcTGestioneFile.DescCliente.Valore = DataGest.Tables("AdapGest").Rows(0).Item("desc_cliente") & ""
                    End If

                End If

                mcTGestioneFile.Insert()

            End If

        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub


    '    Private Function LeggiCodCliente(ByVal sNomeFile As String) As String

    '        On Error GoTo gesterr

    '        Dim i As Integer
    '        Dim k As Integer
    '        Dim lNrRiga As Integer
    '        Dim lNrZeri As Integer
    '        Dim lNrCar As Integer
    '        Dim sCodCliente As String
    '        Dim sZeriStr As String

    '        sCodCliente = ""

    '        mcTSqlVerticaleGestionale.ReadAll("RIGAFILE")
    '        lNrRiga = mcTSqlVerticaleGestionale.StringaSql.Valore

    '        mcTSqlVerticaleGestionale.ReadAll("LUNGHEZZACODCLIENTE")
    '        lNrCar = mcTSqlVerticaleGestionale.StringaSql.Valore

    '        lNrZeri = 6 - lNrCar

    '        If lNrZeri <> 0 Then
    '            Dim sZeri As New String("0", lNrZeri)
    '            sZeriStr = sZeri
    '        Else
    '            sZeriStr = ""
    '        End If

    '        i = FreeFile()
    '        Microsoft.VisualBasic.FileOpen(i, sNomeFile, OpenMode.Input)

    '        For k = 1 To lNrRiga

    '            If k = lNrRiga Then
    '                sCodCliente = sZeriStr & Mid(LineInput(i), 1, lNrCar)
    '            Else
    '                sCodCliente = LineInput(i)
    '            End If

    '        Next

    '        FileClose(i)
    '        Return sCodCliente

    '        Exit Function
    'gesterr:
    '        FileClose(i)
    '        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    '    End Function

    Private Sub AggiornaGriglie()

        On Error GoTo gesterr

        Dim str As String
        Dim dv As DataView
        Dim oData As DataSet
        Dim oAdap As SqlClient.SqlDataAdapter
        Dim sReparto As String
        Dim sCentro As String

        mcTSqlVerticaleGestionale.ReadAll("CONNDOCMAIL")

        str = mcTGestioneFile.GetGridViewSQL("C")
        oData = New DataSet
        oAdap = New SqlClient.SqlDataAdapter(str, mcTSqlVerticaleGestionale.StringaSql.Valore)
        oAdap.Fill(oData, "Dati")
        dv = New DataView(oData.Tables("Dati"))

        grdFatture.AutoGenerateColumns = False
        grdFatture.DataSource = dv

        str = mcTGestioneFile.GetGridViewSQL("D")
        oData = New DataSet
        oAdap = New SqlClient.SqlDataAdapter(str, mcTSqlVerticaleGestionale.StringaSql.Valore)
        oAdap.Fill(oData, "Dati")
        dv = New DataView(oData.Tables("Dati"))

        grdFattureInvio.AutoGenerateColumns = False
        grdFattureInvio.DataSource = dv

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub tlbEsci_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlbEsci.Click
        Me.Close()
    End Sub

    Private Sub tlbAggiorna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlbAggiorna.Click

        On Error GoTo gesterr

        AggiornaElencoCartelle()
        AggiornaGriglie()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub grdFatture_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdFatture.CellContentClick

        On Error GoTo gesterr

        If grdFatture.Columns(sender.currentcell.columnindex).name = "Aggiungi" Then
            mcTGestioneFile.ReadAll(grdFatture.Rows(e.RowIndex).Cells(mcTGestioneFile.Id.DataField).Value)
            mcTGestioneFile.Stato.Valore = "D"
            mcTGestioneFile.Update(mcTGestioneFile.Id.Valore)
            AggiornaGriglie()
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub grdFattureInvio_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdFattureInvio.CellContentClick

        On Error GoTo gesterr

        If grdFattureInvio.Columns(sender.currentcell.columnindex).name = "Togli" Then
            mcTGestioneFile.ReadAll(grdFattureInvio.Rows(e.RowIndex).Cells(mcTGestioneFile.Id.DataField & "invio").Value)
            mcTGestioneFile.Stato.Valore = "C"
            mcTGestioneFile.Update(mcTGestioneFile.Id.Valore)
            AggiornaGriglie()
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub tlbInvia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlbInvia.Click

        On Error GoTo gesterr

        'Dim oOut As New Microsoft.Office.Interop.Outlook.Application
        'Dim oMail As Microsoft.Office.Interop.Outlook.MailItem     Microsoft.Office.Interop.Outlook.OlItemType.olMailItem

        Dim oOut As Object
        Dim oMail As Object
        Dim i As Integer
        Dim sMsg As String
        Dim sOgg As String
        Dim sFile As String
        Dim sFileDel As String

        mcTSqlVerticaleGestionale.ReadAll("MESSAGGIO")
        sMsg = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("OGGETTO")
        sOgg = mcTSqlVerticaleGestionale.StringaSql.Valore

        oOut = CreateObject("Outlook.Application")

        mcTGestioneFile.ReadAll("D")

        If mcTGestioneFile.RowsCount <> 0 Then
            prg.Minimum = 0
            prg.Maximum = mcTGestioneFile.RowsCount
            prg.Visible = True
            lblDescrizione.Visible = True
        End If

        For i = 1 To mcTGestioneFile.RowsCount

            lblDescrizione.Text = "Invio del file " & Replace(mcTGestioneFile.NomeFile.Valore, "txt", "pdf") & " in corso ..."
            prg.Value = i
            Me.Refresh()

            mcTSqlVerticaleGestionale.ReadAll("CARTELLAFILE")

            sFile = mcTSqlVerticaleGestionale.StringaSql.Valore & Replace(mcTGestioneFile.NomeFile.Valore, "TXT", "PDF")

            If mcTGestioneFile.MailCliente.Valore = "" Then
                mcTGestioneFile.Note.Valore = "MANCA EMAIL"
            Else
                oMail = oOut.CreateItem(0)
                oMail.Body = sMsg
                oMail.Subject = sOgg
                oMail.To = mcTGestioneFile.MailCliente.Valore
                oMail.Attachments.Add(sFile)

                If txtAllegato.Text.Trim <> "" Then oMail.Attachments.Add(txtAllegato.Text)

                oMail.Send()
                oMail = Nothing

                'sFileDel = Dir(Replace(sFile, "pdf", "Con"))
                'If sFileDel <> "" Then My.Computer.FileSystem.DeleteFile(Replace(sFile, "pdf", "Con"))

                'sFileDel = Dir(Replace(sFile, "pdf", "Log"))
                'If sFileDel <> "" Then My.Computer.FileSystem.DeleteFile(Replace(sFile, "pdf", "Log"))

                'sFileDel = Dir(Replace(sFile, "pdf", "lpr"))
                'If sFileDel <> "" Then My.Computer.FileSystem.DeleteFile(Replace(sFile, "pdf", "lpr"))

                'sFileDel = Dir(Replace(sFile, "pdf", "txt"))
                'If sFileDel <> "" Then My.Computer.FileSystem.DeleteFile(Replace(sFile, "pdf", "txt"))

                sFileDel = Dir(sFile)
                If sFileDel <> "" Then My.Computer.FileSystem.DeleteFile(sFile)

                mcTGestioneFile.Note.Valore = "MAIL INVIATA"
                mcTGestioneFile.Stato.Valore = "E"

            End If

            mcTGestioneFile.Update(mcTGestioneFile.Id.Valore)

            mcTGestioneFile.Rows = mcTGestioneFile.Rows + 1

        Next

        prg.Visible = False
        lblDescrizione.Visible = False

        oOut.Quit()
        oOut = Nothing

        AggiornaGriglie()

        Exit Sub
gesterr:
        oOut.Quit()
        oMail = Nothing
        oOut = Nothing
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub tlbSelTutto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlbSelTutto.Click

        On Error GoTo gesterr

        mcTGestioneFile.UpdateAllSpedizione()
        AggiornaGriglie()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub btnSfoglia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSfoglia.Click

        On Error GoTo gesterr

        ofdFile.ShowDialog()
        txtAllegato.Text = ofdFile.FileName

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

End Class