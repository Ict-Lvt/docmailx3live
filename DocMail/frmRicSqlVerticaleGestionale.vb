Public Class frmRicSqlVerticaleGestionale

    Private msNomeForm As String = "frmRicSqlVerticaleGestionale"
    Private mcSQLTSqlVerticaleGestionale As New clsTSqlVerticaleGestionale
    Public msRic As String

    Private Sub frmRicSqlVerticaleGestionale_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        On Error GoTo gesterr

        If msRic.Trim = "" Then
            e.Cancel = False
            Me.Visible = False
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        On Error GoTo gesterr

        msRic = ""
        AggiornaGriglia()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub AggiornaGriglia()

        On Error GoTo gesterr

        Dim str As String
        Dim dv As DataView
        Dim oData As DataSet
        Dim oAdap As SqlClient.SqlDataAdapter

        str = mcSQLTSqlVerticaleGestionale.GetGridViewSQL(txtRicerca.Text)
        oData = New DataSet
        oAdap = New SqlClient.SqlDataAdapter(str, mcSQLTSqlVerticaleGestionale.mDBConn.ConnectionString)
        oAdap.Fill(oData, "Dati")
        dv = New DataView(oData.Tables("Dati"))

        GridMain.AutoGenerateColumns = False
        GridMain.DataSource = dv

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub txtRicerca_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRicerca.TextChanged

        On Error GoTo gesterr

        AggiornaGriglia()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub GridMain_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridMain.CellDoubleClick

        On Error GoTo gesterr

        msRic = GridMain.Rows(e.RowIndex).Cells(mcSQLTSqlVerticaleGestionale.NomeChiave.DataField).Value
        Me.Visible = False

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

End Class