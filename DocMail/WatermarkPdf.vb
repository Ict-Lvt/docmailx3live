﻿Imports Mlc.Pdf
Public Class WatermarkPdf
    Private _sqlLayoutLoader As SqlLayoutDataLoader

    Public Sub New()
        _sqlLayoutLoader = New SqlLayoutDataLoader(New SqlClient.SqlConnectionStringBuilder() With {.DataSource = "ServerDbCent\DB001",
                                                                        .InitialCatalog = "LiveWatermark",
                                                                        .UserID = "WtmUser",
                                                                        .Password = "Dev@test!19"})
    End Sub

    Public Sub Process(watermarkDataFile As WatermarkDataFile)
        Using oWatermarkMgr As New WatermarkMgr(_sqlLayoutLoader, "LVH001", False)
            oWatermarkMgr.ProcessWatermarks(watermarkDataFile)
        End Using
    End Sub
End Class
