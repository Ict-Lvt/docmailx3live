<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSqlVerticaleGestionale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtStringaSql = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDescrizione = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNomeChiave = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tlsUtenti = New System.Windows.Forms.ToolStrip()
        Me.tlbNuovo = New System.Windows.Forms.ToolStripButton()
        Me.tlbSalva = New System.Windows.Forms.ToolStripButton()
        Me.tlbElimina = New System.Windows.Forms.ToolStripButton()
        Me.tlbCerca = New System.Windows.Forms.ToolStripButton()
        Me.tlbEsci = New System.Windows.Forms.ToolStripButton()
        Me.tlsUtenti.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtStringaSql
        '
        Me.txtStringaSql.AllowDrop = True
        Me.txtStringaSql.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStringaSql.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStringaSql.Location = New System.Drawing.Point(95, 102)
        Me.txtStringaSql.MaxLength = 0
        Me.txtStringaSql.Multiline = True
        Me.txtStringaSql.Name = "txtStringaSql"
        Me.txtStringaSql.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtStringaSql.Size = New System.Drawing.Size(378, 89)
        Me.txtStringaSql.TabIndex = 73
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(115, 16)
        Me.Label3.TabIndex = 72
        Me.Label3.Text = "Stringa Sql. . . . . . . . ."
        '
        'txtDescrizione
        '
        Me.txtDescrizione.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescrizione.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescrizione.Location = New System.Drawing.Point(95, 73)
        Me.txtDescrizione.MaxLength = 250
        Me.txtDescrizione.Name = "txtDescrizione"
        Me.txtDescrizione.Size = New System.Drawing.Size(378, 21)
        Me.txtDescrizione.TabIndex = 71
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(139, 16)
        Me.Label5.TabIndex = 70
        Me.Label5.Text = "Descrizione . . . . . . . . . . . ."
        '
        'txtNomeChiave
        '
        Me.txtNomeChiave.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNomeChiave.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomeChiave.Location = New System.Drawing.Point(95, 46)
        Me.txtNomeChiave.MaxLength = 50
        Me.txtNomeChiave.Name = "txtNomeChiave"
        Me.txtNomeChiave.Size = New System.Drawing.Size(126, 21)
        Me.txtNomeChiave.TabIndex = 69
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 16)
        Me.Label1.TabIndex = 68
        Me.Label1.Text = "Nome Chiave. . . . . . . . . . . . . . ."
        '
        'tlsUtenti
        '
        Me.tlsUtenti.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.tlsUtenti.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tlbNuovo, Me.tlbSalva, Me.tlbElimina, Me.tlbCerca, Me.tlbEsci})
        Me.tlsUtenti.Location = New System.Drawing.Point(0, 0)
        Me.tlsUtenti.Name = "tlsUtenti"
        Me.tlsUtenti.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tlsUtenti.Size = New System.Drawing.Size(480, 39)
        Me.tlsUtenti.Stretch = True
        Me.tlsUtenti.TabIndex = 67
        Me.tlsUtenti.Text = "ToolStrip1"
        '
        'tlbNuovo
        '
        Me.tlbNuovo.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlbNuovo.Image = Global.DocMailX3.My.Resources.Resources._2
        Me.tlbNuovo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbNuovo.Name = "tlbNuovo"
        Me.tlbNuovo.Size = New System.Drawing.Size(84, 36)
        Me.tlbNuovo.Text = "Nuovo"
        '
        'tlbSalva
        '
        Me.tlbSalva.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlbSalva.Image = Global.DocMailX3.My.Resources.Resources.FLOPPY_DISC
        Me.tlbSalva.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbSalva.Name = "tlbSalva"
        Me.tlbSalva.Size = New System.Drawing.Size(78, 36)
        Me.tlbSalva.Text = "Salva"
        '
        'tlbElimina
        '
        Me.tlbElimina.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlbElimina.Image = Global.DocMailX3.My.Resources.Resources._39
        Me.tlbElimina.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbElimina.Name = "tlbElimina"
        Me.tlbElimina.Size = New System.Drawing.Size(85, 36)
        Me.tlbElimina.Text = "Elimina"
        '
        'tlbCerca
        '
        Me.tlbCerca.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlbCerca.Image = Global.DocMailX3.My.Resources.Resources.kghostview
        Me.tlbCerca.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbCerca.Name = "tlbCerca"
        Me.tlbCerca.Size = New System.Drawing.Size(81, 36)
        Me.tlbCerca.Text = "Cerca"
        '
        'tlbEsci
        '
        Me.tlbEsci.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlbEsci.Image = Global.DocMailX3.My.Resources.Resources.Run44
        Me.tlbEsci.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbEsci.Name = "tlbEsci"
        Me.tlbEsci.Size = New System.Drawing.Size(66, 36)
        Me.tlbEsci.Text = "Esci"
        '
        'frmSqlVerticaleGestionale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(480, 196)
        Me.Controls.Add(Me.txtStringaSql)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDescrizione)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtNomeChiave)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tlsUtenti)
        Me.Name = "frmSqlVerticaleGestionale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestione Parametri del programma"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tlsUtenti.ResumeLayout(False)
        Me.tlsUtenti.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtStringaSql As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDescrizione As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNomeChiave As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tlbNuovo As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlbSalva As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlbElimina As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlbCerca As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlbEsci As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlsUtenti As System.Windows.Forms.ToolStrip
End Class
