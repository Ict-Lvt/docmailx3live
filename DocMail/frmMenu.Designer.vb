<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnParametri = New System.Windows.Forms.Button()
        Me.btnMandaMail = New System.Windows.Forms.Button()
        Me.btnEsci = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnMandaMailOac = New System.Windows.Forms.Button()
        Me.btnSpedisciRDO = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "SPEDISCI ORDINI"
        '
        'btnParametri
        '
        Me.btnParametri.Image = Global.DocMailX3.My.Resources.Resources.view_text
        Me.btnParametri.Location = New System.Drawing.Point(416, 34)
        Me.btnParametri.Name = "btnParametri"
        Me.btnParametri.Size = New System.Drawing.Size(175, 142)
        Me.btnParametri.TabIndex = 1
        Me.btnParametri.UseVisualStyleBackColor = True
        '
        'btnMandaMail
        '
        Me.btnMandaMail.BackColor = System.Drawing.Color.Red
        Me.btnMandaMail.Image = Global.DocMailX3.My.Resources.Resources.CRISTAL_MAIL
        Me.btnMandaMail.Location = New System.Drawing.Point(12, 34)
        Me.btnMandaMail.Name = "btnMandaMail"
        Me.btnMandaMail.Size = New System.Drawing.Size(175, 142)
        Me.btnMandaMail.TabIndex = 0
        Me.btnMandaMail.UseVisualStyleBackColor = False
        Me.btnMandaMail.Visible = False
        '
        'btnEsci
        '
        Me.btnEsci.Image = Global.DocMailX3.My.Resources.Resources.Run44
        Me.btnEsci.Location = New System.Drawing.Point(597, 34)
        Me.btnEsci.Name = "btnEsci"
        Me.btnEsci.Size = New System.Drawing.Size(175, 142)
        Me.btnEsci.TabIndex = 3
        Me.btnEsci.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(413, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(145, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "PARAMETRI GENERALI"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(594, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "ESCI"
        '
        'btnMandaMailOac
        '
        Me.btnMandaMailOac.Image = Global.DocMailX3.My.Resources.Resources.CRISTAL_MAIL
        Me.btnMandaMailOac.Location = New System.Drawing.Point(13, 34)
        Me.btnMandaMailOac.Name = "btnMandaMailOac"
        Me.btnMandaMailOac.Size = New System.Drawing.Size(175, 142)
        Me.btnMandaMailOac.TabIndex = 6
        Me.btnMandaMailOac.UseVisualStyleBackColor = True
        '
        'btnSpedisciRDO
        '
        Me.btnSpedisciRDO.Image = Global.DocMailX3.My.Resources.Resources.CRISTAL_MAIL
        Me.btnSpedisciRDO.Location = New System.Drawing.Point(193, 34)
        Me.btnSpedisciRDO.Name = "btnSpedisciRDO"
        Me.btnSpedisciRDO.Size = New System.Drawing.Size(175, 142)
        Me.btnSpedisciRDO.TabIndex = 8
        Me.btnSpedisciRDO.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(192, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(95, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "SPEDISCI RDO"
        '
        'frmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 187)
        Me.Controls.Add(Me.btnSpedisciRDO)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnMandaMailOac)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnEsci)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnParametri)
        Me.Controls.Add(Me.btnMandaMail)
        Me.Name = "frmMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DocMail 1.0 - Software per l'invio automatico delle fatture tramite mail"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnMandaMail As System.Windows.Forms.Button
    Friend WithEvents btnParametri As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnEsci As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnMandaMailOac As Button
    Friend WithEvents btnSpedisciRDO As Button
    Friend WithEvents Label4 As Label
End Class
