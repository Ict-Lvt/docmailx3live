﻿Imports System.Data.SqlClient
Imports System.IO.Compression
Imports Microsoft.Win32
Public Class frmMandaMailOac


    Private msNomeForm As String = "frmMandaMailOac"
    Private mcTGestioneFile As New clsTGestioneFile
    Private mcTSqlVerticaleGestionale As New clsTSqlVerticaleGestionale

    Private Sub frmMandaMail_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        On Error GoTo gesterr

        mcTGestioneFile.CloseConnection()
        mcTSqlVerticaleGestionale.CloseConnection()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub frmMandaMail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        On Error GoTo gesterr

        mcTGestioneFile.OpenConnection()
        mcTSqlVerticaleGestionale.OpenConnection()

        mcTGestioneFile.Delete("C")
        mcTGestioneFile.Delete("D")

        Me.Focus()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub AggiornaElencoCartelle()

        On Error GoTo gesterr

        Dim j As Long
        Dim k As Long
        Dim i As Long
        Dim lFineGriglia As Long
        Dim lNrFile As Long
        Dim sCartella As String
        Dim sFile As String
        Dim sCodFornitore As String
        Dim lLunghezzaCodice As Long
        Dim lInizioCodice As Long

        mcTGestioneFile.Delete("C")
        mcTGestioneFile.Delete("D")
        mcTSqlVerticaleGestionale.ReadAll("CARTELLAFILEOAC")

        If mcTSqlVerticaleGestionale.RowsCount = 0 Then
            Exit Sub
        Else
            sCartella = mcTSqlVerticaleGestionale.StringaSql.Valore & "*.pdf"
        End If

        mcTSqlVerticaleGestionale.ReadAll("LUNGHEZZACODFORNITORE")
        lLunghezzaCodice = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("INIZIOCODICEOAC")
        lInizioCodice = mcTSqlVerticaleGestionale.StringaSql.Valore

        'MessageBox.Show("Cartella" & vbCrLf & sCartella)

        sFile = Dir(sCartella)

        While sFile <> ""
            'MessageBox.Show("File pdf" & vbCrLf & sFile)
            lNrFile = lNrFile + 1
            sFile = Dir()
        End While

        prg.Minimum = 0
        prg.Maximum = lNrFile
        prg.Visible = True
        lblDescrizione.Visible = True

        sFile = Dir(sCartella)

        For i = 0 To lNrFile - 1

            lblDescrizione.Text = "Lettura e conversione del file " & sFile & " in corso ..."
            prg.Value = i + 1
            Me.Refresh()

            sCodFornitore = Mid(sFile, lInizioCodice, lLunghezzaCodice)
            LeggiEdAggiornaFornitore(sCodFornitore, sFile)

            sFile = Dir()

            'MessageBox.Show(sCodFornitore & vbCrLf & sFile)
        Next

        AggiornaGriglie()

        prg.Visible = False
        lblDescrizione.Visible = False

        If grdFatture.Rows.Count > 0 Then
            MessageBox.Show("File ordini di acquisto elaborati: " & grdFatture.Rows.Count,
                            "",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information)
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub LeggiEdAggiornaCliente(ByVal sCodCliente As String, ByVal sNomeFile As String)

        On Error GoTo gesterr

        Dim sSqlConn As String = ""
        Dim sSql As String = ""
        Dim DataGest As DataSet = New DataSet("DataGest")
        Dim AdapGest As OleDb.OleDbDataAdapter
        Dim DBConnComm As OleDb.OleDbConnection

        'Leggo Stringa di connessione
        mcTSqlVerticaleGestionale.ReadAll("CONN")

        If mcTSqlVerticaleGestionale.RowsCount() > 0 Then
            sSqlConn = mcTSqlVerticaleGestionale.StringaSql.Valore
            DBConnComm = New OleDb.OleDbConnection(sSqlConn)
            DBConnComm.Open()

            mcTGestioneFile.CodCliente.Valore = sCodCliente
            mcTGestioneFile.Stato.Valore = "C"
            mcTGestioneFile.NomeFile.Valore = sNomeFile

            mcTSqlVerticaleGestionale.ReadAll("RMAILCLIENTE")

            If mcTSqlVerticaleGestionale.RowsCount() > 0 Then
                sSql = mcTSqlVerticaleGestionale.StringaSql.Valore
                sSql = Replace(sSql, "@CodCliente", sCodCliente)

                AdapGest = New OleDb.OleDbDataAdapter(sSql, DBConnComm)
                AdapGest.Fill(DataGest, "AdapGest")

                If DataGest.Tables("AdapGest").Rows.Count = 0 Then
                    mcTGestioneFile.MailCliente.Valore = ""
                    mcTGestioneFile.DescCliente.Valore = ""
                    mcTGestioneFile.Note.Valore = "Codice Cliente non presente sul gestionale"
                Else
                    mcTGestioneFile.MailCliente.Valore = DataGest.Tables("AdapGest").Rows(0).Item("email_cliente") & ""
                    'mcTGestioneFile.DescCliente.Valore = DataGest.Tables("AdapGest").Rows(0).Item("desc_cliente")
                    mcTGestioneFile.Note.Valore = ""
                    If mcTGestioneFile.MailCliente.Valore = "" Then mcTGestioneFile.Note.Valore = "Indirizzo email non presente per il cliente"
                End If

                AdapGest = Nothing
                DataGest.Clear()

                mcTSqlVerticaleGestionale.ReadAll("RDESCCLIENTE")

                If mcTSqlVerticaleGestionale.RowsCount() > 0 Then
                    sSql = mcTSqlVerticaleGestionale.StringaSql.Valore
                    sSql = Replace(sSql, "@CodCliente", sCodCliente)

                    AdapGest = New OleDb.OleDbDataAdapter(sSql, DBConnComm)
                    AdapGest.Fill(DataGest, "AdapGest")

                    If DataGest.Tables("AdapGest").Rows.Count = 0 Then
                        mcTGestioneFile.DescCliente.Valore = ""
                        mcTGestioneFile.Note.Valore = "Codice Cliente non presente sul gestionale"
                    Else
                        mcTGestioneFile.DescCliente.Valore = DataGest.Tables("AdapGest").Rows(0).Item("desc_cliente") & ""
                    End If

                End If

                mcTGestioneFile.Insert()

            End If
            DBConnComm.Close()
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub LeggiEdAggiornaFornitore(ByVal sCodFornitore As String, ByVal sNomeFile As String)

        On Error GoTo gesterr

        Dim sSqlConn As String = ""
        Dim sSql As String = ""
        Dim DataGest As DataSet = New DataSet("DataGest")
        Dim AdapGest As OleDb.OleDbDataAdapter
        Dim DBConnComm As OleDb.OleDbConnection

        'Leggo Stringa di connessione
        mcTSqlVerticaleGestionale.ReadAll("CONN")

        If mcTSqlVerticaleGestionale.RowsCount() > 0 Then
            sSqlConn = mcTSqlVerticaleGestionale.StringaSql.Valore
            DBConnComm = New OleDb.OleDbConnection(sSqlConn)
            DBConnComm.Open()

            mcTGestioneFile.CodCliente.Valore = sCodFornitore
            mcTGestioneFile.Stato.Valore = "C"
            mcTGestioneFile.NomeFile.Valore = sNomeFile

            mcTSqlVerticaleGestionale.ReadAll("RMAILFORNITORE")

            If mcTSqlVerticaleGestionale.RowsCount() > 0 Then
                sSql = mcTSqlVerticaleGestionale.StringaSql.Valore
                sSql = Replace(sSql, "@CodFornitore", sCodFornitore)

                AdapGest = New OleDb.OleDbDataAdapter(sSql, DBConnComm)
                AdapGest.Fill(DataGest, "AdapGest")

                If DataGest.Tables("AdapGest").Rows.Count = 0 Then
                    mcTGestioneFile.MailCliente.Valore = ""
                    mcTGestioneFile.DescCliente.Valore = ""
                    mcTGestioneFile.Note.Valore = "Codice Fornitore non presente sul gestionale"
                Else

                    mcTGestioneFile.MailCliente.Valore = ""

                    mcTGestioneFile.MailCliente.Valore += IIf(DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore1").trim <> "", DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore1").trim & ";", "")
                    mcTGestioneFile.MailCliente.Valore += IIf(DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore2").trim <> "", DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore2").trim & ";", "")
                    mcTGestioneFile.MailCliente.Valore += IIf(DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore3").trim <> "", DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore3").trim & ";", "")
                    mcTGestioneFile.MailCliente.Valore += IIf(DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore4").trim <> "", DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore4").trim & ";", "")
                    mcTGestioneFile.MailCliente.Valore += IIf(DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore5").trim <> "", DataGest.Tables("AdapGest").Rows(0).Item("email_fornitore5").trim & ";", "")

                    mcTGestioneFile.Note.Valore = ""

                    If mcTGestioneFile.MailCliente.Valore = "" Then
                        mcTGestioneFile.Note.Valore = "Indirizzo email non presente per il fornitore"
                    Else
                        'tolgo l'ultimo punto e virgola
                        mcTGestioneFile.MailCliente.Valore = Mid(mcTGestioneFile.MailCliente.Valore, 1, Len(mcTGestioneFile.MailCliente.Valore) - 1)
                    End If

                End If

                AdapGest = Nothing
                DataGest.Clear()

                mcTSqlVerticaleGestionale.ReadAll("RDESCFORNITORE")

                If mcTSqlVerticaleGestionale.RowsCount() > 0 Then
                    sSql = mcTSqlVerticaleGestionale.StringaSql.Valore
                    sSql = Replace(sSql, "@CodFornitore", sCodFornitore)

                    AdapGest = New OleDb.OleDbDataAdapter(sSql, DBConnComm)
                    AdapGest.Fill(DataGest, "AdapGest")

                    If DataGest.Tables("AdapGest").Rows.Count = 0 Then
                        mcTGestioneFile.DescCliente.Valore = ""
                        mcTGestioneFile.Note.Valore = "Codice Fornitore non presente sul gestionale"
                    Else
                        mcTGestioneFile.DescCliente.Valore = DataGest.Tables("AdapGest").Rows(0).Item("desc_fornitore") & ""
                    End If

                End If

                mcTGestioneFile.Insert()

            End If
            DBConnComm.Close()
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub


    Private Sub AggiornaGriglie()

        On Error GoTo gesterr

        Dim str As String
        Dim dv As DataView
        Dim oData As DataSet
        Dim oAdap As SqlClient.SqlDataAdapter
        Dim sReparto As String
        Dim sCentro As String

        mcTSqlVerticaleGestionale.ReadAll("CONNDOCMAIL")

        str = mcTGestioneFile.GetGridViewSQL("C")
        oData = New DataSet
        oAdap = New SqlClient.SqlDataAdapter(str, mcTSqlVerticaleGestionale.StringaSql.Valore)
        oAdap.Fill(oData, "Dati")
        dv = New DataView(oData.Tables("Dati"))

        grdFatture.AutoGenerateColumns = False
        grdFatture.DataSource = dv

        str = mcTGestioneFile.GetGridViewSQL("D")
        oData = New DataSet
        oAdap = New SqlClient.SqlDataAdapter(str, mcTSqlVerticaleGestionale.StringaSql.Valore)
        oAdap.Fill(oData, "Dati")
        dv = New DataView(oData.Tables("Dati"))

        grdFattureInvio.AutoGenerateColumns = False
        grdFattureInvio.DataSource = dv

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub tlbEsci_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlbEsci.Click
        Me.Close()
    End Sub

    Private Sub tlbAggiorna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlbAggiorna.Click

        On Error GoTo gesterr

        AggiornaElencoCartelle()
        AggiornaGriglie()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub grdFatture_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdFatture.CellContentClick

        On Error GoTo gesterr
        Dim sCartellaDestPacchetti As String

        mcTSqlVerticaleGestionale.ReadAll("CARTELLADESTFILE")
        sCartellaDestPacchetti = mcTSqlVerticaleGestionale.StringaSql.Valore

        If grdFatture.Columns(sender.currentcell.columnindex).name = "Aggiungi" Then
            'If IO.File.Exists(sCartellaDestPacchetti & grdFatture.Rows(e.RowIndex).Cells(nome_file.Index).Value.ToString.Replace(".pdf", ".zip")) Then
            mcTGestioneFile.ReadAll(grdFatture.Rows(e.RowIndex).Cells(mcTGestioneFile.Id.DataField).Value)
            mcTGestioneFile.Stato.Valore = "D"
            mcTGestioneFile.Update(mcTGestioneFile.Id.Valore)
            AggiornaGriglie()
            'Else
            '    MessageBox.Show("Pacchetto non ancora generato." & vbCrLf & "Impossibile inserire in coda di invio.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'End If
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub grdFattureInvio_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdFattureInvio.CellContentClick

        On Error GoTo gesterr

        If grdFattureInvio.Columns(sender.currentcell.columnindex).name = "Togli" Then
            mcTGestioneFile.ReadAll(grdFattureInvio.Rows(e.RowIndex).Cells(mcTGestioneFile.Id.DataField & "invio").Value)
            mcTGestioneFile.Stato.Valore = "C"
            mcTGestioneFile.Update(mcTGestioneFile.Id.Valore)
            AggiornaGriglie()
        End If

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub tlbInvia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlbInvia.Click

        On Error GoTo gesterr

        If grdFattureInvio.Rows.Count = 0 Then
            Exit Sub
        End If


        Dim oOut As Object
        Dim oMail As Object
        Dim i As Integer
        Dim sMsg As String
        Dim sOgg As String
        Dim sFile As String
        Dim sFileDel As String
        Dim sCartellaDestPacchetti As String
        Dim sZipFile As String
        Dim sCartellaDestOAC As String
        Dim sMittenteMail As String

        Dim bDebug As Boolean
        Dim sMailDebug As String

        Dim sConn As String
        Dim odtOAC As DataTable
        Dim oAdapter As OleDb.OleDbDataAdapter
        Dim sStrSqlRighOAC As String


        mcTSqlVerticaleGestionale.ReadAll("CONN")
        sConn = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("RIGHEOAC")
        sStrSqlRighOAC = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("CARTELLADESTFILE")
        sCartellaDestPacchetti = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("MESSAGGIOOAC")
        sMsg = mcTSqlVerticaleGestionale.StringaSql.Valore

        'mcTSqlVerticaleGestionale.ReadAll("OGGETTOOAC")
        'sOgg = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("CARTELLADESTOAC")
        sCartellaDestOAC = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("MITTENTEMAIL")
        sMittenteMail = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("DEBUG")
        bDebug = Convert.ToBoolean(mcTSqlVerticaleGestionale.StringaSql.Valore)

        mcTSqlVerticaleGestionale.ReadAll("MAILDESTDEBUG")
        sMailDebug = mcTSqlVerticaleGestionale.StringaSql.Valore

        oOut = CreateObject("Outlook.Application")

        mcTGestioneFile.ReadAll("D")

        If mcTGestioneFile.RowsCount <> 0 Then
            prg.Minimum = 0
            prg.Maximum = mcTGestioneFile.RowsCount
            prg.Visible = True
            lblDescrizione.Visible = True
        End If

        For i = 1 To mcTGestioneFile.RowsCount

            lblDescrizione.Text = "Invio del file " & Replace(mcTGestioneFile.NomeFile.Valore, "txt", "pdf") & " in corso ..."
            prg.Value = i
            Me.Refresh()

            mcTSqlVerticaleGestionale.ReadAll("CARTELLAFILEOAC")

            sFile = mcTSqlVerticaleGestionale.StringaSql.Valore & Replace(mcTGestioneFile.NomeFile.Valore, "TXT", "PDF")
            sZipFile = sCartellaDestPacchetti & IO.Path.GetFileNameWithoutExtension(mcTGestioneFile.NomeFile.Valore) & ".zip"

            If mcTGestioneFile.MailCliente.Valore = "" Then
                mcTGestioneFile.Note.Valore = "MANCA EMAIL"
            Else

                Dim sCommessa As String = ""
                oAdapter = New OleDb.OleDbDataAdapter(sStrSqlRighOAC.Replace("@OAC", mcTGestioneFile.NomeFile.Valore.Split("_")(0)), sConn)
                odtOAC = New DataTable()
                oAdapter.Fill(odtOAC)

                For Each oRow As DataRow In odtOAC.Rows
                    If sCommessa = "" Then
                        sCommessa = oRow("commessa").ToString()
                    Else
                        If sCommessa <> oRow("commessa").ToString() Then
                            sCommessa = "VARIE"
                        End If
                    End If
                Next

                'My.Computer.FileSystem.MoveFile(sFile, sCartellaDestOAC & Now.Year & "\ORDINI " & Now.Year & "\" & IO.Path.GetFileNameWithoutExtension(sFile) & "_" & mcTGestioneFile.DescCliente.Valore.Replace("'", "").Replace(" ", "") & ".pdf")
                Dim sNomeOrdine As String = "ORDINE " & Convert.ToInt32(mcTGestioneFile.NomeFile.Valore.Substring(8, 4)) & " " & mcTGestioneFile.DescCliente.Valore.Replace("'", "") & " C_" & sCommessa

                sOgg = sNomeOrdine

                oMail = oOut.CreateItem(0)
                For Each oAccount As Object In oOut.Session.Accounts
                    ' When the e-mail address matches, return the account.
                    If oAccount.SmtpAddress = sMittenteMail Then
                        oMail.SendUsingAccount = oAccount
                    End If
                Next
                'oMail.GetInspector() 'Firma Mail
                'oMail.Body = sMsg 'Testo Mail 
                oMail.HtmlBody = sMsg 'Testo Html Mail
                oMail.Subject = sOgg
                If bDebug Then
                    oMail.To = sMailDebug
                Else
                    oMail.To = mcTGestioneFile.MailCliente.Valore
                End If
                oMail.Attachments.Add(sFile)
                If txtAllegato.Text.Trim <> "" Then
                    oMail.Attachments.Add(txtAllegato.Text)
                End If

                If IO.File.Exists(sZipFile) Then
                    oMail.Attachments.Add(sZipFile)
                End If

                oMail.Send()
                oMail = Nothing


                My.Computer.FileSystem.MoveFile(sFile, sCartellaDestOAC & Now.Year & "\ORDINI " & Now.Year & "\" & sNomeOrdine & ".pdf", True)


                'sFileDel = Dir(sFile)
                'If sFileDel <> "" Then My.Computer.FileSystem.DeleteFile(sFile)

                mcTGestioneFile.Note.Valore = "MAIL INVIATA"
                mcTGestioneFile.Stato.Valore = "E"

            End If

            mcTGestioneFile.Update(mcTGestioneFile.Id.Valore)

            mcTGestioneFile.Rows = mcTGestioneFile.Rows + 1

        Next

        prg.Visible = False
        lblDescrizione.Visible = False

        oOut.Quit()
        oOut = Nothing

        AggiornaGriglie()

        MessageBox.Show("Mail inviate.",
                        "",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information)

        Exit Sub
gesterr:
        oOut.Quit()
        oMail = Nothing
        oOut = Nothing
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub tlbSelTutto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlbSelTutto.Click

        On Error GoTo gesterr

        mcTGestioneFile.UpdateAllSpedizione()
        AggiornaGriglie()

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub btnSfoglia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSfoglia.Click

        On Error GoTo gesterr

        ofdFile.ShowDialog()
        txtAllegato.Text = ofdFile.FileName

        Exit Sub
gesterr:
        MsgBox(Err.Number & " " & Err.Description & Chr(13) & msNomeForm)

    End Sub

    Private Sub tlbGeneraPacchetti_Click(sender As Object, e As EventArgs) Handles tlbGeneraPacchetti.Click
        Try
            Dim sCartellaFileOAC As String
            Dim sCartellaDestPacchetti As String
            Dim sFileNonTrovati As String

            mcTSqlVerticaleGestionale.ReadAll("CARTELLAFILEOAC")
            sCartellaFileOAC = mcTSqlVerticaleGestionale.StringaSql.Valore

            mcTSqlVerticaleGestionale.ReadAll("CARTELLADESTFILE")
            sCartellaDestPacchetti = mcTSqlVerticaleGestionale.StringaSql.Valore


            If sCartellaFileOAC.Trim.Length = 0 And sCartellaDestPacchetti.Trim.Length = 0 Then
                Exit Sub
            Else
                prg.Value = 0
                prg.Minimum = 0
                prg.Maximum = My.Computer.FileSystem.GetFiles(sCartellaFileOAC, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf").Count
                prg.Visible = True
                lblDescrizione.Visible = True
                sFileNonTrovati = ""
                For Each sFilePath As String In My.Computer.FileSystem.GetFiles(sCartellaFileOAC, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                    Dim fileInfo As IO.FileInfo
                    fileInfo = New IO.FileInfo(sFilePath)

                    lblDescrizione.Text = "Lettura e conversione del file " & fileInfo.Name & " in corso ..."


                    CreaPacchetto(IO.Path.GetFileNameWithoutExtension(fileInfo.Name).Split("_")(0), IO.Path.GetFileNameWithoutExtension(fileInfo.Name).Split("_")(1), sCartellaDestPacchetti, sFileNonTrovati)

                    prg.Value += 1
                    Me.Refresh()
                Next
                prg.Value = prg.Maximum
                prg.Visible = False
                lblDescrizione.Visible = False

                If sFileNonTrovati.Length > 0 Then
                    MessageBox.Show("File non trovati." & sFileNonTrovati,
                                    "",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information)
                End If

                MessageBox.Show("Pacchetti generati.",
                                "",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information)

                AggiornaElencoCartelle()
                AggiornaGriglie()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message,
                            "Errore",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CreaPacchetto(sOrdineAcq As String, sFornitore As String, sPathDest As String, ByRef sFileNonTrovati As String)
        Dim oZipCompressor As ZipCompressor
        Dim oFileInfo As IO.FileInfo

        Dim odtOAC As DataTable
        Dim oAdapter As OleDb.OleDbDataAdapter

        Dim sPathDisegni As String
        Dim sStrSqlDisegniOAC As String
        Dim sStrSqlFilePdm As String
        Dim sConn As String
        Dim sConnPDM As String
        Dim sVirgola As String = ""


        'Try
        If IO.Directory.Exists(sPathDest & sOrdineAcq & "_" & sFornitore & "\") Then
            IO.Directory.Delete(sPathDest & sOrdineAcq & "_" & sFornitore & "\", True)
        End If

        If IO.File.Exists(sPathDest & sOrdineAcq & "_" & sFornitore & ".zip") Then
            IO.File.Delete(sPathDest & sOrdineAcq & "_" & sFornitore & ".zip")
        End If

        mcTSqlVerticaleGestionale.ReadAll("CONN")
        sConn = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("CONNPDM")
        sConnPDM = mcTSqlVerticaleGestionale.StringaSql.Valore

        mcTSqlVerticaleGestionale.ReadAll("PATHDISEGNI")
        sPathDisegni = mcTSqlVerticaleGestionale.StringaSql.Valore

        'PROCEDURA OAC
        mcTSqlVerticaleGestionale.ReadAll("DISEGNIOAC")
        sStrSqlDisegniOAC = mcTSqlVerticaleGestionale.StringaSql.Valore.Replace("@OAC", sOrdineAcq)


        oAdapter = New OleDb.OleDbDataAdapter(sStrSqlDisegniOAC, sConn)
        odtOAC = New DataTable()
        oAdapter.Fill(odtOAC)

        If odtOAC.Rows.Count > 0 Then

            sStrSqlFilePdm = "DECLARE @Guid nvarchar(36) = CONVERT(nvarchar(36), NEWID())

INSERT INTO dbo.list_related_doc
(
  Guid
,PdmCode
,PdmConfig
,OrderCode
,Quantity
,ErpOrder
,Execution
)
VALUES "

            For Each oRow As DataRow In odtOAC.Rows
                sStrSqlFilePdm &= sVirgola & "(@Guid," &
                    "'" & oRow("cod_pdm").ToString() & "'" & "," &
                    "'" & oRow("configurazione").ToString() & "'" & "," &
                    "'" & oRow("commessa").ToString() & "'" & "," &
                    Convert.ToDouble(oRow("qta_tot")) & "," &
                    "'" & sOrdineAcq & "'" & "," &
                    "'A DISEGNO'" &
                    ")" & vbCrLf

                sVirgola = ","
            Next

            sStrSqlFilePdm &= vbCrLf & "EXECUTE dbo.sp_get_pdm_related_doc @Guid"

            'MessageBox.Show(sStrSqlFilePdm)
            'IO.File.WriteAllText(Application.StartupPath & "\logsp.txt", sStrSqlFilePdm)

            Dim oConnPdm As New OleDb.OleDbConnection(sConnPDM)
            oConnPdm.Open()
            Dim oCommand As New OleDb.OleDbCommand(sStrSqlFilePdm, oConnPdm)
            Dim oReader As OleDb.OleDbDataReader = oCommand.ExecuteReader()
            'oReader.GetSchemaTable()

            If oReader.HasRows Then
                While oReader.Read
                    'oReader.GetString(oReader.GetOrdinal("Folder3D"))
                    'oReader.GetString(oReader.GetOrdinal("FileJpg"))
                    'oReader.GetString(oReader.GetOrdinal("FileStep"))
                    'oReader.GetString(oReader.GetOrdinal("Folder2D"))
                    'oReader.GetString(oReader.GetOrdinal("FileDxf"))
                    'oReader.GetString(oReader.GetOrdinal("FilePdf"))


                    ''DEBUG
                    'If oReader.GetString(oReader.GetOrdinal("PdmCode")) = "LP25953S" Then
                    '    If IO.File.Exists(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder3D")) & oReader.GetString(oReader.GetOrdinal("FileStep"))) Then
                    '        MessageBox.Show("File esiste")
                    '        If Not IO.File.Exists(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("FileStep"))) Then

                    '            MessageBox.Show("File non esiste in destinazione")

                    '            My.Computer.FileSystem.CopyFile(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder3D")) & oReader.GetString(oReader.GetOrdinal("FileStep")),
                    '                                            sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FileStep")))
                    '            MessageBox.Show("File copiato")

                    '            oFileInfo = New IO.FileInfo(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FileStep")))
                    '            oFileInfo.IsReadOnly = False

                    '            MessageBox.Show("permessi modificati")
                    '        End If
                    '    End If
                    'End If
                    'MessageBox.Show("Reading....")
                    'MessageBox.Show("Deve esistere" & vbCrLf & sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder3D")) & oReader.GetString(oReader.GetOrdinal("FileStep")))
                    'MessageBox.Show("Se non esiste copio" & vbCrLf & sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("FileStep")))
                    'MessageBox.Show("Copio con questo path" & vbCrLf & sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FileStep")))
                    'MessageBox.Show(oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim.Replace("\", "").Replace("/", "").Replace(":", "").Replace("*", "").Replace("?", "").Replace("""", "").Replace("<", "").Replace(">", "").Replace("|", ""))


                    If IO.File.Exists(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder3D")) & oReader.GetString(oReader.GetOrdinal("FileStep"))) Then
                        If Not IO.File.Exists(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("FileStep"))) Then
                            My.Computer.FileSystem.CopyFile(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder3D")) & oReader.GetString(oReader.GetOrdinal("FileStep")),
                                                            sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FileStep")))
                            oFileInfo = New IO.FileInfo(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FileStep")))
                            oFileInfo.IsReadOnly = False
                        End If
                    Else
                        sFileNonTrovati &= vbCrLf & sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder3D")) & oReader.GetString(oReader.GetOrdinal("FileStep"))
                    End If

                    'MessageBox.Show("Step")

                    If IO.File.Exists(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder2D")) & oReader.GetString(oReader.GetOrdinal("FileDxf"))) Then
                        If Not IO.File.Exists(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("FileDxf"))) Then
                            My.Computer.FileSystem.CopyFile(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder2D")) & oReader.GetString(oReader.GetOrdinal("FileDxf")),
                                                            sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FileDxf")))
                            oFileInfo = New IO.FileInfo(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FileDxf")))
                            oFileInfo.IsReadOnly = False
                        End If
                    Else
                        sFileNonTrovati &= vbCrLf & sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder2D")) & oReader.GetString(oReader.GetOrdinal("FileDxf"))
                    End If
                    'MessageBox.Show("Dxf")

                    'COPIA FILE PDF BASICA
                    If IO.File.Exists(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder2D")) & oReader.GetString(oReader.GetOrdinal("FilePdf"))) Then
                        If Not IO.File.Exists(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("FilePdf"))) Then
                            My.Computer.FileSystem.CopyFile(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder2D")) & oReader.GetString(oReader.GetOrdinal("FilePdf")),
                                                            sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FilePdf")))
                            oFileInfo = New IO.FileInfo(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FilePdf")))
                            oFileInfo.IsReadOnly = False
                        End If
                    Else
                        sFileNonTrovati &= vbCrLf & sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder2D")) & oReader.GetString(oReader.GetOrdinal("FilePdf"))
                    End If

                    'MessageBox.Show("Pdf")



                    ''COPIA FILE PDF CON WATERMARKS
                    'If IO.File.Exists(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder2D")) & oReader.GetString(oReader.GetOrdinal("FilePdf"))) Then
                    '    If Not IO.File.Exists(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("FilePdf"))) Then
                    '        Dim oWatermarkPdf As New WatermarkPdf
                    '        Dim oDataFile As New Mlc.Pdf.WatermarkDataFile(sPathDisegni & oReader.GetString(oReader.GetOrdinal("Folder2D")) & oReader.GetString(oReader.GetOrdinal("FilePdf")),
                    '                                                       sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FileDxf")))

                    '        oDataFile.Add("Order", oReader.GetString(oReader.GetOrdinal("OrderCode")))
                    '        oDataFile.Add("Quantity", oReader.GetDouble(oReader.GetOrdinal("Quantity")))
                    '        oDataFile.Add("ErpOrder", oReader.GetString(oReader.GetOrdinal("ErpOrder")))
                    '        oDataFile.Add("Execution", oReader.GetString(oReader.GetOrdinal("Execution")))
                    '        oDataFile.Add("MrpCode", oReader.GetString(oReader.GetOrdinal("MrpCode")))
                    '        oDataFile.Add("QRCode", oReader.GetString(oReader.GetOrdinal("OrderCode")) & "," &
                    '                      oReader.GetString(oReader.GetOrdinal("ErpOrder")) & "," &
                    '                      oReader.GetString(oReader.GetOrdinal("MrpCode")))

                    '        oWatermarkPdf.Process(oDataFile)

                    '        oFileInfo = New IO.FileInfo(sPathDest & sOrdineAcq & "_" & sFornitore & "\" & oReader.GetString(oReader.GetOrdinal("MrpCode")) & "_" & oReader.GetString(oReader.GetOrdinal("Descrizione")).Trim & "_" & oReader.GetString(oReader.GetOrdinal("FilePdf")))
                    '        oFileInfo.IsReadOnly = False
                    '    End If
                    'End If

                    ''MAURO
                    ''PROVA STATICA DEL FILE PDF CON WATERMARKS
                    'If IO.File.Exists("C:\Users\Simone\Documents\Visual Studio 2017\Projects\Livetech\[20191206] PdfWatermarkSample\PdfWatermarkSample\PDF Archive\A4.pdf") Then
                    '    Dim oWatermarkPdf As New WatermarkPdf
                    '    Dim oDataFile As New Mlc.Pdf.WatermarkDataFile("C:\Users\Simone\Documents\Visual Studio 2017\Projects\Livetech\[20191206] PdfWatermarkSample\PdfWatermarkSample\PDF Archive\A4.pdf",
                    '                                                       "C:\Users\Simone\Documents\Visual Studio 2017\Projects\Livetech\[20191206] PdfWatermarkSample\PdfWatermarkSample\bin\Debug\PDF Output\A4.pdf")

                    '    oDataFile.Add("Order", "LV200123")
                    '    oDataFile.Add("Quantity", 100)
                    '    oDataFile.Add("ErpOrder", "IT20OAC-1234")
                    '    oDataFile.Add("Execution", "A DISEGNO")
                    '    oDataFile.Add("MrpCode", "LPD000002D-0100-00")
                    '    oDataFile.Add("QRCode", "LV200123,IT20OAC-1234,LPD000002D-0100-00")

                    '    oWatermarkPdf.Process(oDataFile)
                    'End If
                End While

                oConnPdm.Close()

                If IO.Directory.Exists(sPathDest & sOrdineAcq & "_" & sFornitore & "\") Then
                    oZipCompressor = New ZipCompressor()

                    oZipCompressor.Compress(sPathDest & sOrdineAcq & "_" & sFornitore, sPathDest & sOrdineAcq & "_" & sFornitore & ".zip")
                End If

                If IO.Directory.Exists(sPathDest & sOrdineAcq & "_" & sFornitore & "\") Then
                    IO.Directory.Delete(sPathDest & sOrdineAcq & "_" & sFornitore & "\", True)
                End If

            End If
        End If
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message & vbCrLf & ex.StackTrace,
        '    "Errore",
        '    MessageBoxButtons.OK,
        '    MessageBoxIcon.Error)
        'End Try

    End Sub


End Class